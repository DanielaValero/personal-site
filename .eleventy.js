const blockImagePlugin = require("markdown-it-block-image");
const pluginRss = require("@11ty/eleventy-plugin-rss");
const pluginSyntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const inclusiveLangPlugin = require("@11ty/eleventy-plugin-inclusive-language");
const pluginTOC = require('eleventy-plugin-toc')
const readingTime = require('eleventy-plugin-reading-time');
const filters = require('./scripts/filters.js');
const njkFilters = require('./scripts/njkFilters.js');

const transforms = require('./scripts/transforms.js');
const markdownIt = require("markdown-it");
const anchor = require('markdown-it-anchor');
const blogTools = require("eleventy-plugin-blog-tools");

/***
 * Available: https://plug11ty.com/plugins/blog-tools/
 */

module.exports = function (eleventyConfig) {
  // eleventyConfig.setServerOptions({
  //   port: 3000
  // })
  /**
   * Add layout aliases
   * @link https://www.11ty.dev/docs/layouts/#layout-aliasing
   */
  eleventyConfig.addLayoutAlias("post", "layouts/post.njk");
  eleventyConfig.addLayoutAlias('base', 'layouts/base.njk')
  eleventyConfig.addLayoutAlias('page', 'layouts/page.njk')

  /**
   * Plugins
   * @link https://www.11ty.dev/docs/plugins/
   */

  eleventyConfig.addPlugin(pluginRss);
  eleventyConfig.addPlugin(pluginSyntaxHighlight);
  eleventyConfig.addPlugin(readingTime);
  eleventyConfig.addPlugin(pluginTOC, {
    tags: ["h2", "h3", "h4"],
    wrapper: "nav",
    ul: true,
    wrapperClass: "toc__list"
  })
  eleventyConfig.addPlugin(blogTools);
  //eleventyConfig.addPlugin(inclusiveLangPlugin);


  /**
   * Filters
   * @link https://www.11ty.io/docs/filters/
   */
  Object.keys(filters).forEach((filterName) => {
    eleventyConfig.addFilter(filterName, filters[filterName])
  })


  Object.keys(njkFilters).forEach((filterName) => {
    eleventyConfig.addNunjucksAsyncFilter(filterName, njkFilters[filterName])
  })



  /**
   * Transforms
   * @link https://www.11ty.io/docs/config/#transforms
   */
  Object.keys(transforms).forEach((transformName) => {
    eleventyConfig.addTransform(transformName, transforms[transformName])
  })


  /**
   * Collections
   * ============================
   */
  eleventyConfig.addCollection("posts", function (collection) {
    return collection.getAllSorted().filter(function (item) {
      return item.inputPath.match(/^\.\/posts\//) !== null;
    });
  });

  eleventyConfig.addCollection("digest", function (collection) {
    return collection.getAllSorted().filter(function (item) {
      return item.inputPath.match(/^\.\/digest\//) !== null;
    });
  });

  // TAGLIST used from the official eleventy-base-blog  https://github.com/11ty/eleventy-base-blog/blob/master/.eleventy.js
  eleventyConfig.addCollection('tagList', function (collection) {
    let tagSet = new Set()
    collection.getAll().forEach(function (item) {
      if ('tags' in item.data) {
        let tags = item.data.tags

        tags = tags.filter(function (item) {
          switch (item) {
            // this list should match the `filter` list in tags.njk
            case 'digest':
            case 'pages':
            case 'post':
              return false
          }

          return true
        })

        for (const tag of tags) {
          tagSet.add(tag)
        }
      }
    })

    // returning an array in addCollection works in Eleventy 0.5.3
    return [...tagSet]
  })



  /**
   * Passthrough File Copy
   * @link https://www.11ty.dev/docs/copy/
   */
  eleventyConfig.addPassthroughCopy("static/img");
  eleventyConfig.addPassthroughCopy("static/icons");
  eleventyConfig.addPassthroughCopy("static/fonts");
  eleventyConfig.addPassthroughCopy("static/files");

  eleventyConfig.addPassthroughCopy("admin");
  // eleventyConfig.addPassthroughCopy("_includes/assets/js")
  // eleventyConfig.addPassthroughCopy("_includes/assets/css")


  eleventyConfig.addWatchTarget('src/css/**/*.css');

  /**
   * Set custom markdown library instance...
   * and support for Emojis in markdown...
   * ...because why not control our .MD files and have Emojis built in?
   * @link https://www.11ty.dev/docs/languages/markdown/#optional-set-your-own-library-instance
   * @link https://www.npmjs.com/package/markdown-it-emoji
   *
   */

  /* Markdown Plugins */

  let options = {
    html: true,
    breaks: true,
    linkify: true
  };

  let optsAnchor = {
    permalink: anchor.permalink.linkAfterHeader({
      style: 'visually-hidden',
      assistiveText: title => `Permalink to “${title}”`,
      visuallyHiddenClass: 'sr-only',
      symbol: ""
    })
  };

  const optionsImage = {
    outputContainer: 'div',
    containerClassName: "u-photo"
  };

  eleventyConfig.setLibrary("md",
    markdownIt(options)
    .use(anchor, optsAnchor)
    .use(blockImagePlugin, optionsImage)
  );

  return {
    templateFormats: ["md", "njk", "html", "liquid"],
    // If your site lives in a different subdirectory, change this.
    // Leading or trailing slashes are all normalized away, so don’t worry about it.
    // If you don’t have a subdirectory, use "" or "/" (they do the same thing)
    // This is only used for URLs (it does not affect your file structure)
    pathPrefix: "/",
    markdownTemplateEngine: "liquid",
    htmlTemplateEngine: "njk",
    dataTemplateEngine: "njk",
    passthroughFileCopy: true,
    dir: {
      input: ".",
      includes: "_includes",
      data: "_data",
      output: "public"
    }
  };
};