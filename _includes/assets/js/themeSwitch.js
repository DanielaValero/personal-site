
 

(function () {
  function loadStoredTheme() {
    const theme = localStorage.getItem("theme");
   
    if (theme) {
      const themeSelector = document.querySelector('#'+theme+'')
      document.body.setAttribute('data-theme', theme);
      themeSelector.checked = true;
     
    }
  }
 
  function listenForThemeChanges () {

    document.addEventListener("keydown", (event) => {
      // tab
      if (event.keyCode === 9) {
        document.querySelector(".skip-link-header").classList.remove("sr-only");
      }

      //esc
      if (event.keyCode === 27) {
        document.querySelector(".skip-link-header").classList.add("sr-only");
      }
    })
    // body

    const themes = document.querySelectorAll('[name=theme]');

    themes.forEach((theme)=> {
      theme.addEventListener('change', function (event) {
        if (event.target.value ==='dark')  {
          document.body.setAttribute('data-theme', 'dark');
          localStorage.setItem("theme", "dark")
        } else {
          document.body.removeAttribute('data-theme');
          localStorage.removeItem("theme")
        }
      });
    })
  }

  listenForThemeChanges();
  loadStoredTheme();
})();
