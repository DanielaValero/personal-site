document.addEventListener("DOMContentLoaded", function() {
  var webMentionImages = [].slice.call(document.querySelectorAll('.webmentions__face'));
  var lazyImages = [].concat(webMentionImages);

  if ("IntersectionObserver" in window && lazyImages.length > 0) {
    
    let lazyImageObserver = new IntersectionObserver(function(entries, observer) {
      entries.forEach(function(entry) {
        if (entry.isIntersecting) {
          let lazyImage = entry.target;
          const hasPlaceholder = lazyImage.dataset.src.indexOf('-placeholder') !== -1;
          lazyImage.src = hasPlaceholder ? lazyImage.dataset.src.replace('-placeholder','') : lazyImage.dataset.src;
          lazyImage.classList.remove("lazy");
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });

    lazyImages.forEach(function(lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  }
});
