---
title: 'How to keep growing a team''s technical knowledge in 2020? (or any year) '
date: 2020-01-05T16:10:35.551Z
summary: >-
  When leading technical teams, one of the most important things we need to take
  care is to grow the team skills by introducing knowledge, patterns,
  techniques, that are relevant for the team, the product and that the people
  are ready to understand.
tags:
  - tech lead
  - knowledge management
featuredimage: /static/img/600x400-learning-people-small.jpg
imageAltText: learning people
syndicationTitle: dev.to
syndicationLink: >-
  https://dev.to/danielavalero/how-to-keep-growing-a-team-s-technical-knowledge-118g
---
![learning people](/static/img/1600x500-learning-people-big.jpeg)

When leading technical teams, one of the most important things we need to take care is to grow the team skills by introducing knowledge, patterns, techniques, that are relevant for the team, the product and that the people are ready to understand.



**On finding the right balance**

When one person in the team introduces a pattern, or a paradigm, that not everyone is ready to handle, regardless of how strong the solution is, this pattern would introduce a constrain for the other team members on maintaining that part of the code.

Simplicity is on the eyes of the beholder. A pattern can seem very simple for a developer who is very experienced with it. But for the developer who is not familiar with it, the same pattern can be really complex or impossible to understand.

As a technical lead, the decision making in this regard is not an easy one. How to find the balance between two opposite points? What is simple for the most experienced dev, is complex for the less experienced dev (at that particular thing).

Keeping everything simple for the less experienced devs, might involve creating solutions that are not the best for a specific problem. On the other hand, giving room for the most experienced dev to introduce a pattern that are unknown by the rest, would potentially create a stronger technical solution, but would limit the less experienced devs to maintain that piece of code.

**The balance**

At this point, I tend to get to the idea of: Introducing new patterns slowly, growing the skills of the whole team as a unison while creating a space where the knowledge is openly shared, so that we all learn and grow at unison.

If I were to split it in bullet points I'd say:

* **Build and Communicate the ultimate tech goal:** As a tech lead is important that you have a technical goal and that you communicate it open and transparently to the team, if you can build it together even better. If you don't have a goal yet, then answering the following questions might help: Where should the team be? What should we be able to do?
* **Deconstruct the goal it in steps and prioritize them with the team:** Sit together when is necessary, and see what small steps the team needs to take in order to get to that goal. It can be for example: *"We want to do TDD"*, the steps might be: *"Write a unit test for every code written"*, then *"Write strong tests upon which we can rely"*.
* **The technical lead is not the holder of the holy grail:** As a technical lead you don't need to know everything, master every little piece of knowledge that exists in Software. Your role is to give room for people to be able to apply their knowledge, while sharing it and growing the rest of the team members. Just be humble about what you know, and don't pretend, be honest, this is the best way to keep learning yourself!
* **Define a metric to know when the team is ready to move on:** To keep things interesting, moving and evolving, is important for you and the team to know: "How do we know that we can move on to the next thing?". Having a metric is what will help you to keep moving forward.
* **Temporal inconsistency is fine:** During the time of learning and practicing a new pattern, there will be inconsistency in the code base, some parts will be written in the old way, others in the new way. Once the team masters the new pattern, the old way written code will be refactored with the time.

**Tip:** If you have a technical goal, and don't know where to start, unit testing is the answer. Having a strong unit test base, is what will allow you to refactor and move things around without fear of regressions and breaking working things.

If you will remember something of this blogpost, let it be this one: 

The solution is always in the middle.
