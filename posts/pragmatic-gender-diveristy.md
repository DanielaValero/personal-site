---
title: "Pragmatic Gender diversity: The five things to bring peace to our career\_growth"
date: 2019-01-04T00:00:00.000Z
summary: >-
  How to advance our career as women without feeling the struggle of the gender
  anti-discrimination war
tags:
  - diversity
  - tech
featuredimage: /static/img/together-small-placeholder.png
imageAltText: People working together
syndicationTitle: dev.vo
syndicationLink: >-
  https://dev.to/danielavalero/pragmatic-gender-diversity-the-five-things-to-bring-peace-to-our-career-growth-ief-temp-slug-4621438?preview=0c38ce5e27b7162e46257dc722e03fe5d05f1a1bf12eef1276ca272c8e1a969c8143eb070e70384b8042f3b38973f29df7938788a47dd8853f82ee76
---
![working together](/static/img/together-big.png "Working together")

*Disclaimer:* What I write here is my opinion and it applies to work environments where there is a sane level of gender parity awareness. The content here not necessarily applies to environments where there is real and extreme gender discrimination.

I am an advocate of gender diversity and I like to find ways to empower and inspire women to get rid of their fears and find ways to advance on their careers.

In the movement towards gender diversity, is easy to get too emotional or too sensitive about it. Also, being an advocate of gender diversity has an influence on how others perceive you (oftentimes as an over sensitive person) or treat you (with extra care not to get in "trouble").

Lately I have been re-evaluating the list of things I care about, that steal my mental peace (after reading the book how not to give a f\*\*k). I identified that something from the gender initiatives was stealing my peace. After reading and listening to the ideas of Jordan Peterson, I started to understand that the overly sensitive point of view that easy comes with the gender initiatives was stealing my peace and actually holding me back from advancing in my career.

So, here the list of things that I think we should be focusing on, when we want to evolve and growth in our careers, from the gender diversity point of view.

The five things we should care more to evolve our careers from the gender diversity point of view

1. **Improve our technical skills and find who you are technically:** By technical skills I mean whatever you do for work: lawyer, medical doctor, developer, designer, etc. Read books, follow newsletters, watch videos, join a Meet-Up, find a mentor. Independently of the tool you decide to use to learn, the most important thing is to identify what or which specific areas from your work field lights your fire. What makes you feel more inspired, and towards what you find yourself always going. Once you find it, that is your Niche! Congratulations, now you can start using your preferred tool and go and learn more on this topic. If you look at big personalities in your field, all of them have a niche, having a niche is what actually positions you into an expert area and gives you the push you might be missing.
2. **Understand which is your Emotional Intelligence IQ, and improve it:** We have emotions, and all our emotions tell us something important about what we care about and what we don't. If we are angry about something at work for example, is a sign that something is not right and that we should look at it from a different angle in order to solve the situation. Understanding how to work and improve our emotional intelligence is really important, the more calm we respond to situations the better our responses will be, and the more people will understand the message we are trying to convey. Daniel Goleman is the best known author that shares about this topic, and is a good place to start.
3. **Be honest about how you want your work-life balance to be:** People in management position tend to work more than 40 hours a week, or/and they are flexible with time of their private life to travel, go to meetings, have calls, solve problems. If you don't want to do those compromises, that is the right thing to do and it is fine, it will just mean that you won't give to work more than you actually want.
4. **Improve our self-confidence and self compassion:** We women have the tendency of having low self confidence, and this often gets in our way. When we had more self confidence, we would be able to make more realistic plans and wiser interactions with our colleagues, which would communicate to them that we are actually competent. A big part of working on our self-confidence is also treating ourselves with compassion, this year I encountered this knowledge and actually has helped me in ways that I can't even start counting. The best known author behind this concept is Kristin Neff, and a good place to start is watching her Ted Talk.
5. **Understand that unconscious bias affects everyone:** all the genders in different ways. This is unavoidable, our brains need it in order to be able to work. We can decide to have a mental fight every time it rains, or we can actually make peace with it, and go out with the right clothes and tools so that we can go out without being wet. The same thing is with unconscious bias, there is enough information online for us women to learn what the tendencies are. We can either have a mental fight every time we are judged in a biased way, or we can learn the right tools to respond to those situations kindly and move on.

### What should we stop doing?

* **Blaming men or industries for not evolving our own careers:** The industry, men and what not are not to blame for your lack of career advancement. Doing this blaming in psychology is called external locus of control, and in this particular case is holding you back from growing.
* **Getting too emotional or extremist about gender initiatives:** It is fine to love doing and participating in gender initiatives, but all the extremes are bad. Getting too emotional about this will only make you over sensitive. And to be honest, it does not feel comfortable working with someone who is over sensitive in general, specially about this topics. The reason mainly is, because you never know how the person will take something and make a big fuzz out of that, which might cause unnecessary problems in the team. Which will end mostly on people trying to avoid you.
* **Self-segregation:** There is nothing good coming from self-segregation, the whole idea about diversity is actually working with people with different points of view. The reason is, because they all come together and help each other to find a solution or create an idea that is more solid and creative. If you are in a self-segregation group, this won't happen.
