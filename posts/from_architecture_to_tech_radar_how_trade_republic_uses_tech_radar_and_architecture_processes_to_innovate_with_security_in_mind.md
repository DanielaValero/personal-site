---
title: "From Architecture to Tech Radar: How Trade Republic uses Tech Radar and
  Architecture processes to innovate with security in mind"
date: 2024-06-12T16:13:36.083Z
summary: How as a bank in the fast-paced FinTech industry, we balance innovation
  and security using Tech Radar to standardize technology and a streamlined RFC
  process for critical decisions.
tags:
  - post
featuredimage: /static/img/1_tl8g1g2fro04si_16qvbw.webp
imageAltText: Image with same text as title
syndicationTitle: Trade Republic engineering blog on Medium
syndicationLink: https://engineering.traderepublic.com/from-architecture-to-tech-radar-how-trade-republic-uses-tech-radar-and-architecture-processes-to-a05cf0656341
---
Working in FinTech and building an innovative NeoBank implies that we, as an organization, need to juggle two architectural dimensions that imply acting on opposing forces: speed and security. On the one hand, we need to act quickly to respond with innovative solutions to the fast pace of the world economy and technology. This is essential for us to help our customers build their wealth. On the other hand, we need to prioritize security and compliance to be a reliable partner that our customers can trust with their capital.

At Trade Republic we use several techniques to master this juggle. In this blog post, I will tell you about two of them. The first is our tech radar and how we use it to standardize technologies and techniques. The second is our architecture process and how an architecture decision ends in our tech radar. I will end up showing you the technical solution we put in place to make our tech radar a living document.

# Tech Radar

The tech radar is a visualization tool created by ThoughtWorks to track technology solutions needed by their clients around the world.

Essentially, Tech Radar is a tool to map out the current state of technology that an organization or multiple organizations are using to solve their specific set of problems. It comprises four rings, corresponding to different adoption stages, four quadrants to help categorize the type of technology, and blips, which are the representations of the tracked technologies.

Trade Republic is a product tech organization with around 280 engineers. We focus on building specific solutions within a particular industry, unlike ThoughtWorks. While their tech radar serves as a map of general technology industry trends, ours is a map of the technologies and techniques we use to build our product.

Our product engineering organization is divided into teams per business domain. Each one of our business domains has several teams dedicated to solving particular aspects of that domain

We use agile and autonomy are two delivery principles we use to respond to the fast-moving pace of our problem space. Therefore, we realized we needed tools that help all of our teams to be aligned, while they walk independently. Tech radar is one of those tools. It helps our teams to build on well-thought-out technologies and techniques.

This combination of autonomy, agility, and alignment at an organizational level are catalyzers that allow the surface of standards to help advance our engineering practice without harming velocity.



# Moving technology across rings of the radar

The main goal of the process I will explain you now, is to help us keep the radar up to date while we are busy building a bank. Hence, it is more a recommendation than a mandatory practice. We want the process to adapt to us, and evolve with us, without becoming a burden for delivery speed.

To understand better this flow, let me explain the parts that interact with it:

* A **driver**: is an engineer guiding the transition of the technology or technique across the radar.
* A **need** or a **problem**: is the reason for the movement to happen. Usually is a need to improve a current system or a solution to a technical or product problem. Examples could be: Deprecating a paid technology and moving to an open-source solution to reduce cost, or adopting a new technology to build a specific system or feature needed in the product.
* A **technical decision document**: a document to describe the problem, requirements, and details of the suggested solution. This document serves not only as a record of decisions made but also as a tool to get feedback and provide details about the reasons behind our technology decisions. For this, we use requests for comments (RFCs).
* The **transition**: is the movement along the rings. This is the most time intensive part of the process. Depending on the ring, there can be big implications for your engineering organization.

## Standard flow

In tech radar terms a technology or technique navigates across rings. Each ring represents the life cycle stage of that specific choice in the organization. The standard flow looks as described in the image below.

![How technologies or techniques transition across rings](/static/img/img-1.webp)



A technology or technique that is not on the radar moves in the **assess** ring, where the driver evaluates whether it solves the problem at hand. In this phase, the artifact is the RFC, which I will describe better later in this post.

If the assessment is successful the next stop is to do a **trial**. It can be a PoC or building directly the solution in a small team or project. This allows us to learn if the technology or technique solves the problem. If the trial shows that the solution works, then the technology moves to **adopt**, in which all teams with the same use case or need are encouraged to adopt this solution. This ring also means we will build systems, platforms and DevEx improvements on top of this technology.

Last, there is the **hold** ring. A technology enters it when the trial fails or when there is a product, technical, or business reason to deprecate it. In this last case, there needs to be a replacement alternative in the adopt ring, and all teams using the deprecated technology need to plan the migration to the new technology.

## The shorter flow

However, not always a technology needs to move across all the rings. Sometimes, the trial is unnecessary to help finding a decision. For example a time ago we deprecated `yarn` in our web frontend projects and adopted `npm` or `pnpm`, in these case the trial wasn’t necessary. The transition was therefore shortened to the following image.



![How technologies and techniques can sometimes skip a ring](/static/img/img-2.webp)



# Categories in our radar: Quadrants and engineering functions

Categorizing the technologies and techniques helps provide more clarity for engineers consulting the radar, as well as providing clarity about the applicability of technologies or techniques depending on the engineering function, or type of technology.

The first categorization we use are **quadrants**. These are ways to describe the type of technology. At Trade Republic we use:

* **Languages and frameworks:** for programming languages, software testing, and frameworks.
* **DevOps**: for infrastructure tools we use for CI/CD and what we use to run and monitor our systems.
* **Development tools**: for tooling to aid software development, such as databases, dev tools, SVNs, and
* **Techniques:** for practices, methods, or approaches to employ when building or delivering software. Techniques are not related to ways of working but more about practices we use on top of technologies.

For example, most likely the quadrants development tools and DevOps will apply to all engineering functions, and the platform teams will be the main drivers of the transitions inside these rings.

We added a second layer of categorization to provide more granular visibility to engineers consulting the radar, which is the **engineering function**. Each blip in the tech radar gets a tag: backend, data, web, iOS, Android, SRE, or all functions. This layer of categorization allows engineers to filter out technologies that aren’t relevant to their specific function, giving them a clearer focus and allowing them to find faster the information they are looking.



# The symbiosis between the RFC review process and Tech Radar

The primary source of information for technical decisions at Trade Republic are requests for comments (RFCs) or architecture decision records (ADRs). With these artifacts, we describe solutions to problems or needs, as well as why a technology or a technique is moving across the radar.

Sometimes RFCs can be reviewed or approved within the scope of a team or engineering function, for example, an RFC about CI improvements of a specific team. Other times we can skip writing the RFC, such as the case of the deprecation of yarn.

But in other cases, RFCs we need to get a review, feedback, or approval from a wider audience. When this happens we follow the next process:

![How an idea or a problem becomes an RFC and ends up in Tech Radar](/static/img/img-3.webp)

The review process starts with an initiative or a change with architectural significance, which is a change that will have an impact at the system or architecture level.

Once the author completes the RFC, they seek a review from the architecture forum or architecture review. The first is a space where Senior and Senior+ engineers take part to review, give feedback, or learn about a system change that might affect theirs. The second is more expeditious and happens with a smaller audience.

Selecting the audience where to get feedback, review or approval depends on the type of decision. Important technical decisions affecting multiple teams or org-wide engineering practices need to go through this review process. Once the author completes the decision, they summarize it into a short ADR.

The entire process consists of multiple steps but happens relatively quickly, usually taking anywhere from a few days to one or two weeks. Therefore the author updates tech radar to reflect any relevant change after they publish the ADR.

# Technology to make our tech radar a living documentation

At Trade Republic we are using [Backstage](https://backstage.io/docs/overview/what-is-backstage) as a software catalog to ease discoverability over solutions we are building. On its first version, Tech Radar was a standalone living document, it was good to have it, but it wasn’t easy to find, therefore it wasn’t being the living document we need it to be. That is the reason why we decided to move it to Backstage by making use of [its tech radar plugin](https://backstage.spotify.com/marketplace/spotify/plugin/techradar/). In short, to build our tech radar we use three tools:

* Backstage as a service catalog
* The integrated tech radar with Backstage’s official plugin,
* and RFCs/ADRs.

The official plugin of tech radar for backstage takes as an input a JSON file with all the information needed to render the radar. To build this file we wrote a generator that takes in markdown documents with front matter metadata and outputs the JSON.

Choosing markdown format was key to making tech radar a living documentation because it made it more intuitive for engineers to contribute. We split the content from the source code of tech radar to a separate repository where any engineer can contribute.

A contributing engineer writes their updates one markdown file per technology or technique, and then make a pull request. On merge, the CI runs validations, builds the JSON file, and creates a pull request to the backstage repository to update the data of it. In backstage the changes get automatically approved, merged and deployed.

Our tech radar content repository has the following structure

```
tech-radar-content/
├── blips/
│   └── DevOps/
│   │       ├── grafanaLGTM.md
│   └── Languages-Frameworks/
│   │       ├── typescript.md
│   └── Techniques/
│   └── Tools/
├── generator/
│   └── bin/
│   └── src/
```



The blips folder contains one markdown file per technology or technique in our tech radar, split into folders, one per quadrant.

The template of a blip is as follows:

```yaml
# ID of RadarQuadrant this Entry belongs to
# Required. One of: tools, languages-Frameworks, devOps, techniques
quadrant: Tools

# Optional, but encouraged for guild filtering. If left empty allFunctions will be used
# One or multiple: backend, data, web, sre, ios, android
# Use: allFunctions for technologies that apply to all functions
# Default: allFunctions
engineeringFunction:

# Optional. User-clickable links to provide more information about the Entry
# Optional. Default: "#"
url:

# Required. History of the Entry moving through RadarRing
# Detailed explanation: https://backstage.io/docs/reference/plugin-tech-radar.radarentrysnapshot
timeline:
 
   # ID of RadarRing. Required
   # one of: assess, trial, adopt, hold
 - ringId:
   # Required. Point in time when change happened. Encouraged: Fill the date.
   # Format: YYYY-MM-DD or a valid DateTime.
   date:
   # Optional. Description of change. Default: empty
   description: description of change
   # Optional. Indicates trend compared to previous snapshot
   # Default: No Change. 
   # One of: 0 (for no change). 1 (for moved up). -1 (for moved down)
   moved: 0
---


# <Name of entry here>
Description of the technology

# Use case [optional]
Optional but encouraged. Use case of a technology, where and in which cases this technology should be used

# RFC [optional but encouraged]

RFC is an abbreviation for Request For Comments, for more info - https://en.wikipedia.org/wiki/Request_for_Comments
Optional but encouraged. Placeholder to provide RFC link whenever and whereever applicable.
```

## Learning more

If you aren’t familiar with the concept of tech radars, I recommend checking out this [blog post by Andrey Novikov](https://medium.com/@AndreyNovikov/technology-radar-for-technology-strategy-what-is-it-and-how-to-build-it-ec4dcb8ce554).

# Parting words and learnings

Last but not least, I want to share with you some of the key learnings from the process of adopting and evolving how we use Tech Radar, and how to combine it with our architectural process:

1. **Define a clear and well-documented process** to enable any engineer to onboard and contribute easily.
2. **Streamline the contribution and deployment process to make it as simple as possible**. Automating content validation, shortening the number of approvals, and simplifying the deployment process is crucial to encourage engineers to contribute.
3. **Tailor the process to fit the people.** The process should enable the organization’s members to walk toward the same goal. Our current process is the result of several iterations and will continue to evolve in the future. We evolve and improve the process so that it assists us, not to control us.
4. **Keep the focus on the goal:** our technology choices are motivated by what our customers need and the products we want to build. Therefore keeping tech radar up to date is a challenge that we tackle by having tech leads enforce it. Additionally, we give monthly or quarterly updates about the latest changes in tech radar.

Balancing speed, innovation, and security is complex for engineering organizations. Combining these two tools has been a way for us to provide transparency, guidance, and explicit alignment.