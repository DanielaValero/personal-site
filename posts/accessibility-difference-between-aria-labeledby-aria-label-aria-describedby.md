---
title: >-
  Accessibility: What is the difference between aria-labeledby, aria-label and
  aria-describedby?
date: 2019-09-13T14:09:34.269Z
summary: >-
  One of the things I see confuses devs more often when starting with a11y is to
  understand the difference between aria-labeledby, aria-label and
  aria-describedby. Here I explain briefly the difference, so that we use them
  in a more meaningful way.
tags:
  - post
  - accessibility
featuredimage: /static/img/204-small-placeholder.jpg
imageAltText: Naming and describing for screen readers
---

![Naming and describing things for screen readers](/static/img/204-big.jpg)

### Before you start

The first rule of WAI-ARIA is: Not to use WAI-ARIA. The best, more maintainable and future proof experience is provided by using standard HTML elements.

Using WAI-ARIA should be reserved for custom components, where there is no native option. [There is good list of examples in the w3c site, with explanations of how to use](https://www.w3.org/TR/wai-aria-practices/#aria_ex)

### Now, the difference

They differ on the type of information they will hold. 

It can be primary or auxiliary information. 

The **primary information** is basically the name of the element. It ismain information of an element, for example the label of an input field. 

The **auxiliary information,** refers to extra information that is necessary to explain further. An example of this would be the validation error of an input field.

Primary information is read first, auxiliary information is read as the last piece of information of the input control. Meaning after its name and role.

Now that you understand this:

#### Aria-Label

The **aria-label** property enables you to name an element with a string that is not visually rendered. 
It is used for **primary information**


Example:

```
<button type="button" aria-label="Close">X</button>
```

#### Aria-Labeledby

The **aria-labelledby** property enables you to reference other elements on the page to define an accessible name.

It is used for **primary information**

Example: 

if you have a "read more link" that is embedded in a text.

```html
<p id="bees">Bees are disappearing rapidly. Here are seven things you can do to help.<a id="bees-read-more" aria-labelledby="bees bees-read-more">Read more...</a></p>
```

#### Aria-Describedby

The **aria-describedby** property enables you to give further instructions to the screen reader user of how to use an element, how to fill it, or what to fix if there is an error. 

It is used for **auxiliary information**

Example:

The validation errors of an input field are \`aria-describedby\`

```html
<label for="username">Username</label><input id="username" name="username" aria-describedby="username-error">
```

```html
<p id="username-error">Fill your username to continue</p>
```



Note that in this example, the name is the content of the label. That is the primary information.

 There the screen reader will say: 

_"Username, input text, fill your username to continue"_

Native types of descriptions are: \`captions\` that are used for images or tables.
