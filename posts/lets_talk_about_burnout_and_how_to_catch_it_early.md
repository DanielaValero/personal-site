---
title: "Part 1: Let's talk about burnout and how to catch it early"
date: 2022-01-30T11:13:49.662Z
summary: >
  Burnout is more common among us than we think, and like any disease, has
  different stages. The most common is early-stage burnout. Its symptoms are
  usually exhaustion or cynicism towards our workplace or colleagues.

  Having a good understanding of burnout, what is, what causes it, how to recover, and how to prevent it, can be a powerful tool that can help us have a healthier work-life balance.
tags:
  - post
  - burnout
  - mental-health
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
Burnout is more common among us than we think, and like any disease, has different stages. The most common is early-stage burnout. Its symptoms are usually exhaustion or cynicism towards our workplace or colleagues.

Having a good understanding of burnout, what is, what causes it, how to recover, and how to prevent it, can be a powerful tool that can help us have a healthier work-life balance.

The topic is wide and has different layers. I will be publishing a series of blog posts in this regard, where I expand on it, to start, let’s define it, and share some tips.



## Define: Burnout

The [World Health Organization](https://www.who.int/news/item/28-05-2019-burn-out-an-occupational-phenomenon-international-classification-of-diseases) recognized burnout as an occupational phenomenon. Yet they don't classify it as a medical condition, it has become so common, that today the WHO labels it as a symdrome, and calls to address it as such, due to its effects on human beings.

> “Burn-out is a syndrome conceptualized as resulting from chronic workplace stress that has not been successfully managed. It is characterized by three dimensions:
>
> * Feelings of energy depletion or exhaustion;
> * Increased mental distance from one’s job, or feelings of negativism or cynicism related to one's job; and
> * Reduced professional efficacy.
>
> Burn-out refers specifically to phenomena in the occupational context and should not be applied to describe experiences in other areas of life.”

### What causes it

Research has shown that burnout is generally a consequence of a dysfunctional relationship between two parties – the individual and the workplace. The individual does not have the support or internal resources to deal with stress, and the organization keeps putting pressure or lacks organizational structures to detect and address it in a timely manner. Among the causes that research has shown to be correlated to burnout are:

1. Unrealistic Deadlines
2. Unmanageable Workloads
3. High-Risk Deployments*
4. Unmanaged technical debt*
5. Unplanned work
6. Lack of Support and Autonomy
7. Miscommunication with Management‍

\* Applies to software development

## Early indicators

> **Exhaustion** is the central symptom of burnout. It comprises profound physical, cognitive, and emotional fatigue that undermines people’s ability to work effectively and feel positive about what they’re doing
>
> **Cynicism**, ... represents an erosion of engagement. It is essentially a way of distancing yourself psychologically from your work. Instead of feeling invested in your assignments, projects, colleagues, customers, and other collaborators, you feel detached, negative, even callous
>
> exhaustion and cynicism are the two primary measures of burnout
>
>  .... 
>
> Thus, a potential early warning sign is the presence of one of these two dimension ... For example, during a period of peak demand, employees may become seriously exhausted, but their cynicism remains low because they can address the demands through effective coping.

Source: [Managing Burnout in the Workplace ](https://www.oreilly.com/library/view/managing-burnout-in/9781843347347/)

## What to do?

If one thing research as shown clearly is, that in order to prevent and combat burnout, actions from both sides have to be taken: the individual side and from the organization side.

Before we can influence our organizations, or decide if we want to do such a thing. we need to be in good mental health.

Therefore, as the first step to coping constructively with early-stage burnout, here are some science-backed up tips that have worked for me.

*Disclaimer: These recommendations would work for the early stages, chronic burnout takes up to 3 years to be cured, and that happens with close therapeutic treatment*

## First 3 steps

* **Take time to unwind, take time off:** If you reach the moment when you can't switch off work from your mind, or you get any of the symptoms (exhaustion or depersonalization), you should slow down, either take less workload or get medical leave.
* **Raise the red flag in your organization:** As important as taking care of yourself, is taking care of others. If this is happening to you, stakes are high that is happening to others in your organization, therefore, talk to your supervisor about this, raise the flag.
* **Self-reflection to identify the cause**: Research shows that when burnout happens, the relationship of the individual and organization to the six strategic areas* of the workplace is imbalanced. Check with yourself which one(s) are causing you more stress.

  \* These are workload, control or autonomy, reward, community, fairness, and values.

## Moving forward

* **Become aware of what stresses you up**: one of the recommendations is to keep a stress diary. This technique might be of great help to keep an eye on what is stressing you, how much, and how you can cope with it.

  The stress diary is a document with a table that has the next columns: date, time, situation, scale, symptoms, efficiency, and reaction
* **Understand and set your boundaries:** every human has a limit, you need to understand where your limits are. Keeping healthy limits is one of the ways you have to keep your mental sanity and continue growing professionally in a sustainable manner. At work, endurance is more important than speed.
* **Change your routines:** 

  * Create a routine that marks the beginning of the workday
  * Create a routine that marks the end of the workday.
  * Exercise daily
  * Cook healthy meals
* **Manage your energy, not your time**: We are workers of the mind, as we are, our productivity not necessarily can be measured in terms of time, but rather in terms of energy. Keeping an eye on and managing how much energy you have, what gives you energy and what drains you is key to preventing burnout.
* **Connect with like-minded people,** having a social support system is one of the elements that research consistently shows as a factor that reduces and prevents stress.

### References

[Managing Burnout in the Workplace](https://www.oreilly.com/library/view/managing-burnout-in/9781843347347/) - ISBN: 9781780634005

[Manage Your Energy, Not Your Time](https://hbsp.harvard.edu/product/R0710B-PDF-ENG) By Tony Schwartz, Catherine McCarthy 

[What are the 5 stages of burnout?  August 20, 2020](https://www.thisiscalmer.com/blog/5-stages-of-burnout)