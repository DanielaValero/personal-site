---
title: The 6 roles of building accessible experiences explained
date: 2019-10-11T16:04:05.852Z
summary: >-
  When we build digital products, there are involved different roles. Each one
  of them has their particular focus area and responsibilities to shape a
  product.


  Each one of the roles has also some implications when it comes to caring for
  accessibility. Regardless of not being clear, or explicitly defined, the
  responsibilities are important to understand, if we want to build inclusive
  web experiences.
tags:
  - post
  - accessibiltity
featuredimage: /static/img/600x400-roles-small.png
imageAltText: Person thinking and smiling
syndicationTitle: dev.to
syndicationLink: >-
  https://dev.to/danielavalero/the-6-roles-of-building-accessible-experiences-explained-1if9
---
![person smiling and thinking](/static/img/1600x500-roles.png)

_Illustration from [Lucas Wakamatsu](https://lucaswakamatsu.com/)_

There are different roles that are involved when building accessible websites.

### 1. The Developer

They select the right semantic elements for what they need to build, they read and understand the difference components and usages of WAI-Aria to select just the right amount of attributes only when is necessary.

### 2. The Visual Designer

They take care that the text is readable in different screen sizes and zoom levels, they choose colors that can be seen and understood regardless of the color blindness disability and they ensure that all the information the user needs is available even if they don’t see colors.

### 3. The Copy Writer

They write and organize content in the page so that everyone can understand it. They avoid complicated words or highly technical terms, so that people who are not specialist or with dyslexia for example can also understand the message. They also select images and write descriptions to the images that are there to convey meaning.

### 4. The UX

They put themselves in the shoes of the user with disabilities, when designing the UX of a site, or when making a judgement call of how a pattern should work, they primarily consider: how does it feel for the user? Is it clear enough? Is it too overwhelming? Do we need to add explanations? Maybe we need less?. They don’t stay close to the book when making judgement calls, they don’t close their eyes and say: let us leave it like this because that is what the standard says. The go one step further, because they understand that sometimes the standard alone is not enough, the standard is just a small part, a beginning. They add more or less details to the UX judging what feels right to the user.

### 5. The QA

They take that whole experience created, and test it with different browsers and screen readers tools. They try to break it, they use it as if they were a user who has no idea of what the “right flow, right order, right input” is.

### 6. The PO

They have the vision of the product, they decide and communicate how important accessibility is. To what level do they need or want to comply, and communicate to stakeholders why this is in there, as a definition of done thing.

## The mix

In the real world, in a common project, most of these roles are present, but they are not included in their role DoD the accessibility part, they build the experiences thinking on users like themselves. Even if they have a temporary disability, there is the tendency where they don’t think even on the rainbow of users that will come to access that site they are building.

What this means is that when a project considers accessibility as a built in thing, it starts by one person, usually the developer, who puts on the hat of all the roles mentioned above, all at once. They explain and communicate with kindness and clarity why this is important, and how can it be achieved as part of the usual day to day work. They become a leader who inspires everyone in the team to care for these users, they make things simpler, find the best solution, finds the time and overall, keeps asking the question, loud and openly: how does this feel for the user with disabilities?

Eventually, the others will start asking the question. Eventually they will get more curious, learn more, advocate more.
The most important thing is, if it is important for your business, and your product (in most of the cases it is), start caring.

### Moral of the story

If you do care, but don’t know how to start, here is your start: learn to use 1 screen reader.
