---
title: 'DBT-Bite: My take on our product focus - what does it mean for web developers?'
date: 2019-05-20T06:43:15.917Z
summary: >-
  Companies are more and more adapting the Product focus, and are going away
  from standard projects. Working with a product mindset seems to be the "How"
  that guides the actions to become better actors within the digital world. 


  But what does this mean for web developers? What does it mean for those who
  have been working in projects? Let me share my take on this.
featuredimage: /static/img/thinking-small-placeholder.png
imageAltText: person thinking
tags:
  - DBT-Bite
  - tech
---

![person thinking](/static/img/thinking-big.png)


Companies are more and more adapting the Product focus, and are going away from standard projects. Working with a product mindset seems to be the “How” that guides the actions to become better actors within the digital world.

But what does this mean for web developers? What does it mean for those who have been working in projects? Let me share my take on this.


Great Engineers and developers are behind the most famous products in the digital landscape, for example Amazon, Facebook, Instagram, Netflix among others. And in many cases, those engineers are as well part of organizations that set trends to shape the web or make decisions on where should we go now depending on how we all developers behave in digital. 

These engineers contribute to open source and keep a curious mind with regards to how they do things. They are kind and open to share, and receive ideas and knowledge.

Moreover, a great product is not written by a single brilliant person, it is made by a team of people who organizational psychology would name High performance Team. 

You might be asking yourself, what makes an engineer great? It is true that I don’t have a certain answer for that, but one thing I know is that one of the elements they have, is that they stay curious.

A sense of curiosity makes them want to step out of their comfort zone, try new things or master current and old ones. And is that mastering that comes after that sense of curiosity that makes them brilliant.

Designers are not hired to put pixels together, they are hired because of their eye, their singular ideas. We developers should start thinking about ourselves in a similar manner. It is that sense of curiosity what makes us stand out, and is that as well what creates more meaningful bonds among our teams.

### How do we start sparking that curiosity?

* Read articles and put those ideas in practice within your projects
* Write blogposts and share knowledge
* Stay eye open to any small opportunity to learn something new, or refresh something old 
* Create spaces where people connect and share ideas
* Go to activities where people get and share ideas
