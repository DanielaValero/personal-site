---
title: 'Interview | Women in Tech: "To drive change for the better, it is
  fundamental that more women gain a foothold in the tech industry."'
date: 2020-04-23T09:38:58.852Z
summary: >
  In our (Entwikler.de) "Women in Tech" series of articles, we present inspiring
  women who have successfully gained a foothold in the IT industry. In focus
  today: Daniela Valero, Senior Engineer Experience Technology at Publicis
  Sapient.
tags:
  - post
  - womenInTech
featuredimage: /static/img/women-in-tech-600x400-small-placeholder.png
imageAltText: computer with coffee and flowers
syndicationTitle: Entwickler.de
syndicationLink: https://entwickler.de/online/karriere/women-in-tech-valero-579932304.html?utm_source=twitter&utm_medium=social&utm_campaign=Diversity%2CPress
---
![working tools of a woman in tech](/static/img/women-in-tech-1600x500-big.png "Image from Foto von Lum3n von Pexels")

**Original interview in German for [Entwikler.de](https://entwickler.de/online/karriere/women-in-tech-valero-579932304.html?utm_source=danielavalero.com&utm_medium=social&utm_campaign=Diversity%2CPress)**

The tech industry is dominated by men - so far, so bad. Slowly but surely, the so-called Boys Club is getting filled by talented women: more and more women are gaining a foothold in the industry.

For this reason, we want to give exciting and inspiring women the opportunity to introduce themselves and tell how and why they chose the way into the tech industry. But issues such as gender prejudice, challenges or funding opportunities are also discussed.

**Since when are you interested in Tech? How did the first contact with Tech come about (parents, school, self-interest, studies etc.)?**

When I was about 15 or 16 years old, we had the subject programming in school and learned to code basic algorithms in Turbo Pascal. From the beginning I was totally enthusiastic about it and loved the subject so much that I wanted to do my friends' homework as well. So I could spend more time at the computer, because we didn't have one at home.

Later on, when I finally decided to study, I was torn between phycology, i.e. the study of algae, and computer science. But I soon realized that I didn't want to live without codes - so in the end the choice was easier than I thought. At university I was involved in a GNU/Linux user group and thus got in contact with the command line and other "nerdy" applications.

**How was your way until your current job? Which different career paths have you taken?**

My career began as a Java developer in a very conservative and traditional software company. The company had rules that made no sense to me. For example, all employees were required to wear suits every day - even the developers, although they were all crammed into a large, windowless room and had no customer contact.

Afterwards I worked as a PHP developer for the Venezuelan government. The work culture there was strongly influenced by politics: Who was the bigger Chavez fan? Who had more "friends" in positions of power? Since I am a staunch opponent of the regime, I changed sides.

My next stage in my career was as a webmaster for Venezuela's largest opposition online newspaper - La Patilla. The website was based on WordPress and I was responsible for all aspects of the technology and the platform - from the helpdesk to the implementation and maintenance of functions to the coordination of the server strategy and scaling. We often had to fend off government DoS attacks because the country was in the middle of a media war.

This was the point when I decided to go abroad. I ended up in a small startup in Bonn that had developed an app for document management. There I developed and maintained the new website and designed an online dashboard for the app's users to configure their settings more efficiently. Everything was based on ClojureScript. Since I had never worked with it before, I enlisted help from the backend team to understand the technology. I learned a lot and enjoyed the multicultural environment.

In 2014, I finally joined Publicis Sapient, a consultancy for digital business transformation, where I still work as a technical manager in the front-end area. I really enjoy my work - not least because of the great team and the exciting technology projects for international clients.

**Are there people (women) who have supported/promoted you? Do you have a role model?**

At Publicis Sapient, three women are my greatest role models: Suse Menne, Alison Walden and Ute Zankl. As Country & Operations Lead, Suse manages the business in Germany, Austria and Switzerland. She is a down-to-earth and cooperative leader who is really good at what she does. Suse has supported me since I joined Publicis Sapient and continues to support me today.

Alison is Director of Experience Technology in our Canadian office in Toronto. She is friendly, humble, a wonderful speaker and a recognized expert in web accessibility. Despite her position as Director on the other side of the world, she always likes to take time out when I ask her for advice. Simply a wonderful woman.

Ute leads the People Strategy department. She actively promotes diversity, is always there for me in difficult moments and stands by my side.

In the tech community I admire Rachel Andrew and Marc Thiele. Rachel because she has achieved so much in her career. She is a renowned CSS expert, member of w3c and editor-in-chief of Smashing Magazine. Marc is the founder of Beyond Tellerand, a diverse event where technology and creativity meet. The environment he creates is inviting and has extremely high quality standards. Marc is always humble, supportive and a great advocate of diversity.

**Has someone put obstacles in your way?**

During my first jobs I was severely discriminated against and harassed. I was told things like: "You look prettier with your mouth shut" or "Just do what I say if you want to live an easier life here". The working environment was characterised by psychological aggression. One day, even a colleague's wife followed me on the bus after work. When I got off and she noticed, she threatened to kill me. The first two years of my career were really very hard.

After that time I was lucky to work in environments where diversity is appreciated and where you can feel safe. But what I am always confronted with is Unconscious Bias. I have worked with male colleagues whose communication was disrespectful to me. On other occasions I was not considered capable of taking on leadership roles due to subconscious prejudices. It was precisely this Unconscious Bias that caused my career to stagnate at times.

Fortunately, I am a resilient person and know when to ask for help. Despite all obstacles, I have so far achieved every goal I set myself. More slowly and with greater effort than people from social or professional majorities, but I have made it.

**Which position do you now hold, in which company? What exactly are you doing/what does your working day look like?**

I currently work as a Senior Experience Technology Engineer at Publicis Sapient. In this position, I was responsible for a project for DHL - the front-end delivery of our team and, in close cooperation with the product owner, the further roadmap - as front-end lead last year.

My daily work routine starts with the usual "Daily". The meeting is actually the only predictable thing, because every day is different. Sometimes I am more concerned with administrative tasks such as planning, maintaining and preparing "stories". Other days I concentrate more on optimizing our working methods and the exchange between stakeholders and team members. Or I work directly with the technology, writing code, renewing pull requests or further developing our technical vision.

I also lead the diversity team in our Cologne office. This team drives initiatives for more diversity and inclusion. Fortunately, we have added many dedicated members over the past year and are there for anyone who needs our help.

**Have you developed something yourself? If so, describe the project to us.**

A few years ago we developed a digital UI library with components for Bosch. The quality standards were the highest we had implemented at Publicis Sapient up to that point. The team I was privileged to lead consisted of highly talented senior developers. It was a tough year, but we exceeded the client's expectations and earned their trust. What we developed was eventually rolled out on various Bosch websites around the world.

**Why are there so few women in the tech industry? What hurdles do women still have to overcome today?**

In my opinion, we women have many natural strengths, but also some qualities that stand in our way: We tend to judge ourselves too harshly, to be self-critical and easily become insecure about our abilities.

Our industry is patriarchally organized and oriented towards men. The negative qualities of being a woman still too often stand in our way. In addition, men often unjustifiably accuse us of being too emotional and of overreacting to trivialities. In addition, men's more pronounced competitive thinking leads to the fact that we women are often - consciously or unconsciously - belittled so that they themselves are better off.

In many cases one has the feeling that we have to act more like a man in order to be successful ourselves. This is an uncomfortable compromise that few women are prepared to make. Others prefer to stay out of it right away or leave the field again quickly.

**Which clichés/stereotypes have you already encountered in relation to "Women in Tech"? What problems does this cause?**

The best way to describe stereotypes is the double-bind dilemma. When women move up into leadership positions, people tend to see them as competent. But they don't like them because they are seen as "iron ladies". If we act cautiously and cooperatively, people tend to like us, but do not see us as technically competent. A dilemma.

So we can be considered competent and not be liked, or incompetent but popular. Finding the balance between these two poles is a journey we must embark on during our career.

The problem is that we find ourselves in a state of constant struggle - which is not bearable in the long run. This is why many women tend not to aspire to management positions in technical professions, because once you get there, the struggle becomes even worse.

**And why should more women work in the tech industry? Would our world look different if more women would work in STEM? What do you think would be the advantages - on a social, professional, cultural and economic level - if there were more women in the tech industry? Would certain sectors benefit particularly from innovation?**

Many studies show that mixed teams that combine both female and male attributes perform better. This has a positive effect on cooperation, well-being, emotional intelligence and psychological security.

In my opinion, the old model of the male software industry is becoming less important every day. Admittedly, this concept is one of the reasons why the industry is where it is today. But for success in the future, male dominance must be broken.

To drive change for the better and to change the way we work in a positive way, it is essential that more women gain a foothold in the tech industry.

**What does the future look like - will the diversity debate soon be history?**

I believe that this debate will continue for many years to come, with varying degrees of intensity. At present, the social sciences show how important diversity and gender parity are for teams. Not only are better results achieved, but innovations are also positively influenced.

Companies are finally beginning to take diversity seriously and make it a priority. However, societies learn and change very slowly. So it will probably be many years before the fight for more diversity is over.

In the past, it was even more difficult for minorities to hold their own. They had to be strong and have the courage to rebel. My generation does the same now. Today, fortunately, there are more men who are committed to diversity. The next generation will have it easier, but the difficult situation of women in the tech industry will certainly continue for a few years.

**Do you have any tips for women who want to enter the tech industry? What should other girls and women know about working in the tech industry?**

Find something you love to do and learn to master it. Keep up to date with the latest technology, find mentors, contribute to open source, create codepens and PoCs, and work on your technological skills Being able to show toughness when it's needed is also a great advantage. It is also important to be open and continuously educate yourself, improve your communication and listening skills, and make good use of female emotional intelligence.

If you are able to attend regular meetings of the Women in Tech community, connect with like-minded people and build a network, you are on the right track. You should also be careful to control your emotions, be respectful and friendly, and give open feedback to others when you are harassed or bullied. If it's not worth it, move on. True to the motto: 

> If you are going through hell, don't stop, but move on. This is not a good place to stop.