---
title: Manifesto of a happy team
date: 2022-02-27T10:43:16.507Z
summary: |-
  * Kindness instead of aggressive behavior or language
  * Celebration of the diversity of thought instead of single-mindedness
  * Mental health focus instead of workaholism
  * Community instead of individualism
  * Psychological safety instead of an environment of fear
  * Collaboration instead of competition
tags:
  - post
  - tech-lead
  - team-work
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
We are kind to each other.

We celebrate different voices and perspectives,

Mental health is one of our key stones,

We take care of each other

We are safe here

Yet, every one of us has a different role, some of which with different amplitude of influence,

We treat each other with respect, and listen,

We treat each other like the professionals and humans we are.

By doing this, we are creating a safe space,

where every one of us can bring our uniqueness and contribute to what we all as a team are building,

a space where we can keep calm, and be a better version of ourselves at work



All this translates to:

* Kindness instead of aggressive behavior or language
* Celebration of the diversity of thought instead of single-mindedness
* Mental health focus instead of workaholism
* Community instead of individualism
* Psychological safety instead of an environment of fear
* Collaboration instead of competition