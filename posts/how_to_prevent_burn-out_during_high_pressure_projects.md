---
title: How to keep calm and carry on in high pressure project times
date: 2021-10-25T11:57:16.923Z
summary: >-
  The pandemic has changed the world and has done it in multiple layers. One of
  them is the increased awareness we all have on the importance of mental health
  and work-life balance.


  I have learned that quiet time is important, me-time is necessary, and are currently in a new discovery of a new balance, extraversion vs introversion, quiet time vs busy time.
tags:
  - post
  - mental-health
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
The pandemic has changed the world and has done it in multiple layers. One of them is the increased awareness we all have on the importance of mental health and work-life balance.

I have learned that quiet time is important, me-time is necessary, and are currently in a new discovery of a new balance, extraversion vs introversion, quiet time vs busy time.

[Anselm Hannemann wrote a nice piece on the topic](https://helloanselm.com/writings/its-time-for-a-healthy-tech-approach), he wonders in his article: What do we need to do, to make building digital products more sustainable for us, the people behind them.

His point is: switching the focus to the people.

> Once we start building things for people, we not only make others happier and healthier, we will be happier as well. There’s nothing as long-lasting and uplifting as someone else who is grateful for what you have done for them. Our own happiness will make us calmer and more healthy.



When we put people's happiness as part of our priorities, we are also putting mental health and well-being right in front of the priority list. And the first step to do this is to revisit your own work time, under the lens of:

* What can I do, so that during my workday, I have time to just do nothing, unwind and relax?
* What rituals can I add to my week, so that I keep an eye on myself in times of high pressure?



## My more well-being focused work week

Currently, I am working on a big and interesting project, which promises a lot of weeks in a state of high pressure and a lot of things to do. In order to make this manageable for me, I have revisited my week, my rituals, and have answered the questions I have shared above.

### Mondays

**Set week goals.** I plan my week and set myself goals for the week. I also make a bullet list of things I will do that day, check the meetings of the week, etc.

### Every day

**Start the day slowly**, drink a coffee or a tea, write in my journal, or do sports (can be yoga, running, or some functional training)

**Have a do-nothing 5 minutes break**. It changes every day when I do it, usually depending on meetings, but I religiously take the time to just do nothing. I look forward to this time very much every day.

**Emergency check-in if necessary at the end of the day**. If there is a situation that drained me more than usual or gave me more than usual energy, I take some time to reflect on it and write down my thoughts about the situation. This helps me to avoid accumulating stress that over time translates into an unhealthy explosion to other people around me.

### Tuesday - Thursday

**Plan the day.** Check my bullet list, and my weekly goals, mark things as done, and plan the ones I will do on that day, check when I have meetings that day.

### Fridays

**Weekly self-check-in,** I block roughly 2 hours to go to a cafe or some neutral place to reflect on my week. write down what gave me energy, what drained me, How I felt during the week, and if there is something I should keep an eye on so that I can address stressful or conflict situations proactively in a healthy manner. I also write down my successes, what I have achieved.

## Conclusion

Living a happy life is important if you are in a similar industry like I do, and building the type of products that I do: we are not saving lives.

This means: Work is not everything, leading a happy life is on us, having enough energy in our personal lives is something that we can do, even in high-pressure times, and this should be a priority for every single one of us.

For me, it has become a priority to focus on my own well-being, so that I can bring the best part of my own self to work, and continue creating a safe environment, where people can thrive.