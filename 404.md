---
layout: layouts/page.njk
title: The dog ate this page
permalink: 404.html
eleventyExcludeFromCollections: true
---

But there are lots of more pages in <a href="{{ '/' | url }}">the home page</a>
<picture >
    <source type="image/webp" srcset="/static/img/layout/dog-ate-it.webp">
    <source type="image/png" srcset="/static/img/layout/dog-ate-it.png">
     <img class="u-photo" 
      src="/static/img/layout/dog-ate-it.png"
      alt="the dog with the page you were looking for">
  </picture>

