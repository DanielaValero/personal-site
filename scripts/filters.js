const {
  DateTime
} = require('luxon')
const slugify = require('slugify');
const CleanCSS = require('clean-css');
const autoprefixer = require('autoprefixer');
const postcss = require('postcss');
const {
  minify
} = require("terser");

module.exports = {
  /**
   * Dates
   * @link https://www.11ty.dev/docs/filters/
   */
  readableDate: (date) => {
    return DateTime.fromJSDate(date).toFormat("dd LLL yyyy");
  },

  machineDate: (date) => {
    return DateTime.fromJSDate(date).toFormat("yyyy-MM-dd");
  },

  dateFromTimestamp: (timestamp) => {
    return DateTime.fromISO(timestamp, {
      zone: 'utc'
    }).toJSDate()
  },

  htmlDateString: (date) => {
    return DateTime.fromJSDate(date, {
      zone: 'utc'
    }).toFormat('yyyy-LL-dd');
  },
  /**
   * Collections
   * @link https://www.11ty.dev/docs/filters/
   */
  // Get the first `n` elements of a collection.
  head: (array, n) => {
    if (n < 0) {
      return array.slice(n);
    }

    return array.slice(0, n);
  },
  /**
   * Pass ` | limit(x)` to a Collection loop to limit the number returned
   * Alt = ` | reverse | limit(x)` to return X most recent
   * Took the following filters from
   * @link https://www.youtube.com/watch?v=wV77GwOY22w&feature=share
   */
  limit: (arr, count = 5) => {
    return arr.slice(0, count)
  },
  slice: (array, limit) => {
    return limit > 0 ? array.slice(0, limit) : array.slice(limit)
  },
  concat: (array1, array2) => {
    return array1.concat(array2)
  },

  /**
   * URLs
   * @link https://www.11ty.dev/docs/filters/
   */
  // webmentionsForUrl: (webmentions, url, allowedTypes) => {
  //   // const allowedTypes = ['mention-of', 'in-reply-to', 'like-of', 'repost-of'];
  //   if (!allowedTypes) {
  //     allowedTypes = ['mention-of', 'in-reply-to'];
  //   } else if (typeof allowedTypes === "string") {
  //     allowedTypes = [allowedTypes];
  //   }
  //   const allowedHTML = {
  //     allowedTags: ['b', 'i', 'em', 'strong', 'a'],
  //     allowedAttributes: {
  //       a: ['href']
  //     }
  //   }
  //   const clean = entry => {
  //     const {
  //       content
  //     } = entry
  //     if (content && content['content-type'] === 'text/html') {
  //       content.value = sanitizeHTML(content.value, allowedHTML)
  //     }
  //     return entry
  //   }
  //   return webmentions
  //     .filter(entry => {
  //       if (!url) {
  //         return true;
  //       }

  //       let hashSplit = entry['wm-target'].split("#");
  //       let queryparamSplit = hashSplit[0].split("?");
  //       let target = queryparamSplit[0];
  //       return target === url;
  //     })
  //     .filter(entry => allowedTypes.includes(entry['wm-property']))
  //     // .filter(entry => !!entry.content)
  //     .map(clean)
  // },
  absoluteUrl: (url, base) => {
    if (!base) {
      base = siteData.url;
    }
    try {
      return (new URL(url, base)).toString();
    } catch (e) {
      console.log(`Trying to convert ${url} to be an absolute url with base ${base} and failed.`);
      return url;
    }
  },
  /**
   // Universal slug filter strips unsafe chars from URLs
   */
  slugify: (string) => {
    return slugify(string, {
      lower: true,
      replacement: '-',
      remove: /[*+~.·,()'"`´%!?¿:@]/g,
    })
  },
  /**
   * Minify && Process
   * @link https://www.11ty.dev/docs/filters/
   */

  postCSS: (code) => postcss([autoprefixer]).process(code).css,

  cssmin: (code) => new CleanCSS({}).minify(code).styles,

}
