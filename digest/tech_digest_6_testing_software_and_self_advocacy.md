---
title: "Tech digest #6: Testing Software and self advocacy"
date: 2022-02-25T08:01:54.736Z
summary: Setting up E2E testing in a Github pipeline - Document your
  accomplishments - Cello mix for the brain
tags:
  - digest
  - testing-software
  - self-advocacy
imageAltText: Fill when you add an image
syndicationTitle: My Substack tech digest
syndicationLink: https://techdigest.substack.com/
---
###### This week:

###### Techy: [Testing Pipeline 101 For Frontend Testing](https://www.smashingmagazine.com/2022/02/testing-pipeline-101-frontend-testing/)

###### Softy: [Brag now, remember later: Document your accomplishments](https://github.com/readme/guides/document-success)

###### Life: [Cello Mix](https://headphonecommute.com/2021/03/24/matt-emery-cello-mix/)

- - -

## Techy

### Testing Software

#### [Testing Pipeline 101 For Frontend Testing](https://www.smashingmagazine.com/2022/02/testing-pipeline-101-frontend-testing/) by Ramona Schwering

If you know me, you know that I love testing software, and the reason for that is: Testing the code that you write, makes you a better engineer.

Why? Because it forces you to think about how you design your code, and over time it helps you create better software design, which translates into being a better engineer.

Practicing actively software testing is not as common as it appears to be, as a matter of fact, most of the developers I’ve worked with, wish to adopt this practice, or have abandoned it.

This is why I liked this article: Easy, short, digested, and my favorite parts: well researched, and with beautiful illustrations.

Ramona explains what are the different types of tests that you can write, and how to set up UI/E2E tests in the pipeline of a Github project.

PS: She shared an article called [the practical test pyramid](https://martinfowler.com/articles/practical-test-pyramid.html) by Ham Vocke. Is directly on my reading list.

[Read the article](https://www.smashingmagazine.com/2022/02/testing-pipeline-101-frontend-testing/)

- - -

## Softy

### Self Advocacy

#### [Brag now, remember later: Document your accomplishments](https://github.com/readme/guides/document-success) by Monica Powell

I stumbled upon this article in [the ReadME project](https://github.com/readme/) from Github. Which is a project to amplify the voices of the open-source community. Not only does the goal of the project touches my heart, but also its beautifully crafted design. (Did you see the typography and the beautiful illustrations?)

Back to the article, is about self-advocacy by Monica Powel. Something that is certainly not easy for me to do, but along with my work experience, the advice of how important it is to make it, has been coming over and over again.

I find Monica’s advice approachable, and worth considering. Along with the article the shares a [template that you can use to document your bragging](https://jvns.ca/blog/brag-documents/#template) or another option, that I found particularly interesting, especially for my love for learning, creating a [GitHub to write down what you have learned](https://github.com/jbranchaud/til/), as in [\#learnInPublic](https://www.learninpublic.org/)

[Read article](https://github.com/readme/guides/document-success)

- - -

## Music

#### **Matt Emery -** [Cello Mix](https://headphonecommute.com/2021/03/24/matt-emery-cello-mix/)

I am what many call melomaniac, as in I love music, and I am listening to music most of the time when I am awake. Within my musical journey, I’ve found that Cello creates an environment to unwind my mind.

I found this beautiful mix, which you can use along to read this, or to work, in Headphone Commute, an independent online resource of candid words on electronic, experimental, and instrumental music.

In words of max about this mix:

> “…This collection of tracks takes you on a journey of the instrument from intimate solo to large cello ensembles and layered pieces, to ambient hypnotic looped, distorted and electronic to emotional and accompanied by the piano or full orchestra.”

[Listen](https://headphonecommute.com/2021/03/24/matt-emery-cello-mix/)

- - -

Thank you for making it this far, this newsletter is an experiment I am doing. I read a lot of technical and nontechnical articles from a myriad of sources every week, and a lot of what I read are wonderful tools that I like to reflect upon again, and more importantly, share with others.

I am still finding the voice, the format, the editorial line of this newsletter, and your feedback would be of wonderful help to achieve that. If you have any, I’d be more than glad to hear it. Reach out :)

Until the next

Daniela