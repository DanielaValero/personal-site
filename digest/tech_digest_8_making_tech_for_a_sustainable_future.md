---
title: "Tech digest #8: making tech for a sustainable future"
date: 2022-03-13T19:42:09.567Z
summary: "Conference: INTERACT - Course: Foundations of Humane tech - tech &
  world: True innovation requires big tech, academia, and government to work
  together - Softy: The 8 elements of flow"
tags:
  - post
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
###### This week:

###### Conference: [INTERACT](https://devinterrupted.com/event/interact/#event)

###### Course: [Foundations of Humane Technology](https://www.humanetech.com/course)

###### Technology & world: **[True innovation requires big tech, academia, and government to work together](https://www.technologyreview.com/2022/02/22/1044916/innovation-tech-academia-government/)**

###### Softy: [The 8 elements of the flow experience](https://positivepsychology.com/mihaly-csikszentmihalyi-father-of-flow/)

###### Art: [Online performance: The Menstrual Ovulatory Cycle](https://dringeblieben.de/videos/the-menstrual-ovulatory-cycle)

This week’s digest is inspired by international woman’s day and learning to make technology that helps the world move towards a more sustainable future.

- - -

## Techy

### Conference

### [INTERACT](https://devinterrupted.com/event/interact/#event): a community-driven conference for Tech leaders

This is a 1-day conference for engineering leaders organized by Dev Interrupted. What I like: The line-up is amazing, starting with the fact there are many women leaders who I look forward to learning from, and second, is community-driven.

[Get ticket](https://devinterrupted.com/event/interact/#event)

### Course

### [Foundations of Humane Technology](https://www.humanetech.com/course)

Made by the center of humane tech, which I have been following since they launched [Social Dilemma](https://www.thesocialdilemma.com/). This is a free, self-paced course to learn the tenets of humane tech: respect human nature, minimize harmful consequences, center on values, create shared understanding, support fairness and justice, and help people thrive.

As tech makers, learning about these ideas, and finding a way to activate these tenets in our day-to-day job, is one step that can make a big difference for the generations to come.

[Enroll](https://assets-global.website-files.com/5f0e1294f002b1bb26e1f304/622a185b1f8fa622b4df0a16_6-Tenets-Infographic_Center-For-Humane-Tech.png)

### [Technology & world: True innovation requires big tech, academia, and government to work together](https://www.technologyreview.com/2022/02/22/1044916/innovation-tech-academia-government/) by [Shirley Ann Jackson](https://www.thehistorymakers.org/biography/shirley-ann-jackson-41)

Shirley Ann Jackson is such an inspiring woman, she is an American physicist, the first African-American woman to have earned a doctorate at the Massachusetts Institute of Technology (MIT), the first woman appointed chair of the U.S. Nuclear Regulatory Commission, 18th resident of Rensselaer Polytechnic Institute (and the first woman to do it), and the first African-American woman elected to the National Academy of Engineering.

In this article, she explains how, and why technology, academia, and government have to work together to find solutions to the fast pace arising problems the world has today, such as the COVID-19 pandemic, or climate change. As tech makers, we need to start informing ourselves about these perspectives, so that we can start being catalyzers for the change the world needs.

[Read the article](https://www.technologyreview.com/2022/02/22/1044916/innovation-tech-academia-government/)

- - -

## Softy

### Flow

### [The 8 elements of the flow experience](https://positivepsychology.com/mihaly-csikszentmihalyi-father-of-flow/) by Mihaly Csikszentmihalyi

Csikszentmihalyi (pronounced Chick–sent–meehayee) describes eight components of the flow experience, which in Csikszentmihalyi words is:

> “a state in which people are so involved in an activity that nothing else seems to matter; the experience is so enjoyable that people will continue to do it even at great cost, for the sheer sake of doing it” (1990).”

The 8 elements describe what happens, or needs to happen for flow to exist. Understanding these elements as an individual contributor, or as a leader, can enable you to be happier in your day-to-day job, or/and enable others to do so.

[https://positivepsychology.com/mihaly-csikszentmihalyi-father-of-flow/](https://www.flowskills.com/the-8-elements-of-flow.html)

- - -

## Art

### [Online performance: The Menstrual Ovulatory Cycle](https://dringeblieben.de/videos/the-menstrual-ovulatory-cycle)

Is a 24-minute video performance on how the menstrual ovulatory cycle works, and the effects it can have on those who have it.

> it contains metaphorical images and emotional representations of each phase corresponding to the hormonal changes. The way each person experiences each phase, the health conditions and variations are multiple and almost unapproachable.

I found it a great piece of work, and I am sharing it here with you because if you relate to it: You might find a good smile, and if you don’t relate to it, nor understand it or the effects it has on women, this can be a great primer.

[Watch](https://dringeblieben.de/videos/the-menstrual-ovulatory-cycle)

- - -

Thank you for making it this far, this is a small experiment I am doing, to read, re-read, learn and share tools that catch my interest on my journey as an engineering manager and as a tech maker for a more sustainable future.

Until the next

Daniela

[I also publish this on my website](https://danielavalero.com/digest/tech-digest-6:-testing-software-and-self-advocacy/)