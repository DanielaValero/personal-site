---
title: "Tech digest #7: Alignment, a11y conference, healthy tech and rest"
date: 2022-03-04T12:55:22.204Z
summary: Alignment, accessibility, healthy tech, and how to rest
tags:
  - post
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
###### This week

###### Teach Lead: [Alignment: Overcoming Internal Sabotage and Digital Product Failure](https://techleadjournal.dev/episodes/78/)

###### Techy: [axe-con - Building accessible experiences](https://www.deque.com/axe-con/)

###### Softy: **[It’s time for a healthy tech approach](https://helloanselm.com/writings/its-time-for-a-healthy-tech-approach)**

###### life: [How to rest well by Alex Soojung-Kim Pang in Psyche magazine](https://psyche.co/guides/how-to-rest-well-and-enjoy-a-more-creative-sustainable-life)

## Tech Lead

### [Alignment: Overcoming Internal Sabotage and Digital Product Failure - Jonathon Hensley](https://techleadjournal.dev/episodes/78/)

The latest episode of Tech lead journal interviewed Jonathon Hensley, author of “Alignment: Overcoming internal sabotage and digital product failure”.

In this episode, he shares a great deal of insight about what alignment is along with a number of ideas to inspire us as leaders in tech.

A quick spoiler on alignment according to Jonathon:

* **Individual alignment:** How does the person know that the work they do matters. The key needed to be passionate and inspired
* **Team alignment:** everybody on the team knows roles, processes, and how they work together.
* **Organizational alignment:** To have a clear and understood vision and goals, understood and shared across all teams. Build teams and empower individuals.
* **Market alignment:** Empathize with the customers of the product, to design something that solves their needs.

One important thing to mention is that building alignment is a continuous process.

- - -

## Techy

### [axe-con - Building accessible experiences](https://www.deque.com/axe-con/)

This is an unusual share, this is a three-day online free conference about building accessible experiences. Has four tracks:

1. Development
2. Design
3. Organizations adopting accessibility
4. and cross-functional accessibility.

As tech makers are important that we take responsibility and actively train ourselves on what we need to do to build products for all the users, and if the philanthropic reason does not speak to you and you live in Europe, [by 2025 the most important digital products will need to be accessible](https://www.mhc.ie/latest/insights/overview-of-the-european-accessibility-act), better start learning now.

[See the schedule](https://www.deque.com/axe-con/schedule/)

- - -

## Softy

### **[It’s time for a healthy tech approach by Anselm Hannemann](https://helloanselm.com/writings/its-time-for-a-healthy-tech-approach)**

The recent world events have changed who we are, people who can satisfy their human needs in somewhat a comfortable manner, have got more aware of the importance of mental health, or simply do something that makes them happier. In the western world, many people are resigning their jobs, seeking that.

I agree with Anselm on the point that the environment within which we work to make tech needs to continue evolving into a direction where wellbeing, community, and collaboration enable people to have a healthy work-life balance and be happier making tech.

[Read article](https://helloanselm.com/writings/its-time-for-a-healthy-tech-approach)

- - -

### [Manifesto of a happy team](https://danielavalero.com/posts/manifesto-of-a-happy-team/)

Last week I was going through my digital notebook and came across what I named: manifesto of a happy team. I wrote these ideas in December 2020, when I was preparing to announce a team split, to a team that was working under a lot of pressure. The manifesto is short prose verbalizing what a happy team does.

Having this verbalized in this form is important for a team because the values are the foundation that keeps people together, especially when times are hard.

[read article](https://danielavalero.com/posts/manifesto-of-a-happy-team/)

- - -

## Life

### [How to rest well by Alex Soojung-Kim Pang in Psyche magazine](https://psyche.co/guides/how-to-rest-well-and-enjoy-a-more-creative-sustainable-life)

At first glance this article feels like yet another “how to have a good night's sleep”, but Alex is talking about rest as a restorative activity, which can be active as well.

> The most restorative forms of rest are active, not passive. Further, rest is a skill: with practice, you can learn to get better at it, and to get more out of it.

Especially when we are facing constant acute stress levels, for example working on tight deadlines, is important to keep connecting ourselves with whatever activity we enjoy doing, something that takes our empty tank of energy, and replenishes it, or in Alex’s words

> it allows us to recharge and stimulate our creativity, and gives us the mental space to cultivate new insights, and even helps us have longer, more sustainable creative lives. Moreover, studies show that good rest is not idleness.

This topic is very present in my present life, and I will be expanding with more detail on what to do as an individual to manage and get out stronger from early stages burnout situations, or constant acute stress situations. But for now, check the article, is golden.

[read article](https://psyche.co/guides/how-to-rest-well-and-enjoy-a-more-creative-sustainable-life)

- - -

**CSS Comic**

[![](https://cdn.substack.com/image/fetch/w_1456,c_limit,f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F1201c87f-f023-4271-818a-3dc67da45dfe_1294x884.png)](https://cdn.substack.com/image/fetch/f_auto,q_auto:good,fl_progressive:steep/https%3A%2F%2Fbucketeer-e05bbc84-baa3-437e-9518-adb32be77984.s3.amazonaws.com%2Fpublic%2Fimages%2F1201c87f-f023-4271-818a-3dc67da45dfe_1294x884.png)

Until the next

Daniela