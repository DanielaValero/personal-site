---
summary: A workflow for PRs - Technical debt - Mentorship diamond - When not to
  DRY - Diagrams as code - Can I include - less absolute positioning
layout: layouts/post.njk
title: "Week's Digest #2: What happened last week in the web?"
date: 2021-09-20T07:45:05.745Z
tags:
  - digest
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
Last week was a busy and people-focused work week for me. Among the things that happened, there was one thing that popped out and I want to share here is the concept of the [stress pressure curve](https://delphis.org.uk/peak-performance/stress-and-the-pressure-performance-curve/)

![stress performance curve diagram](/static/img/delphis-pressure-performance-stress-curve.png)

The important thing to know is: 

There is a line that divides when we are getting too little challenge or too much challenge. If you get too few, you get bored, if you get too much, you get burned out.

Finding the balance for yourself is a work of constant introspection, knowing when to ask for help, and how to set your own boundaries. Finding this for your teammates, if you are in a position of leadership in your team, is a matter of communication skills, team values, having 1:1s, among others.

The bottom line is: Keep an eye on your line, and talk to your team when you’ve crossed the border. And if you are in a leadership position, check on your teammates, listen when they tell you it is too much for them, and offer assistance when need it

## Ideas to practice in my project

### [PR: ship, show, ask for PRs](https://martinfowler.com/articles/ship-show-ask.html)

As a developer, working with teams, one of the practices that I enjoy more doing, bringing, and evangelizing is to make use of Pull Requests. Over time I have worked in teams of multiple sizes, and with different types of developers.

One of the things I like to do, always with a new team, is to talk about how we deal with our pull requests, what problems we think we have, and how do we want to address them.

This article gives a really nice approach on this, which I certainly will propose to my team for us to try (we are already doing a form of this):

> Ship: You do the PR for yourself, and merge it directly without asking for a review.\
>   Works great when:
>
> * I added a feature using an established pattern
> * I fixed an unremarkable bug
> * I updated documentation
> * I improved my code based on your feedback
>
>  Show: You do the PR, explain things, or ask for improvements, but merge directly without waiting for feedback\
>   Works great when:
>
> * I would love your feedback on how this code could be better
> * Look at this new approach or pattern I used
> * I refactored X so now it looks like this
> * What an interesting bug! Look how I fixed it.
>
> Ask: You do the PR, ask for a review and wait for it.\
> Works great when:
>
> * Will this work?
> * How do we feel about this new approach?
> * I need help to make this better, please
> * I'm done for today, will merge tomorrow

### [Reducing technical debt and increasing developer satisfaction](https://www.infoq.com/articles/reduce-technical-debt/)

This is a thorough and well-written article about what is technical debt, how to deal with it, where it comes from, etc.

As a fronted lead, one of the recurring topics that keep my attention is technical debt. I find myself working close to my teammates to identify: when we are introducing technical debt, make a compromise when we will pay it, and when is better to take longer and do the clean solution.

The one thing from this article that spoke to me, and I am currently actively practicing and inviting my teammates to also practice, is to deal with maintenance work frequently.

In the article the author points out to several pros on doing this, that confirm and rationalize the good feeling I have been having after doing this for a couple of months already:

> * **Agile way of working.**
> * **Increased developer happiness.** 
> * **Higher business value through improved time-to-market of new features**. 
> * **You don’t need to ask permission.** 
> * **It’s actually easier to get started.** 

## Career growth

### [Tool for career: The mentorship diamond](https://mikemcquaid.com/2021/09/09/the-mentorship-diamond/)

I have always been in love with my career, I read constantly about what is happening, new ways of working, technology, approaches, computer science, etc. If I look back on the years I have been working on this, I can actually say that I have never got a mentor. I have worked close to some people, with whom I have learned a lot, but there was never a mentorship relationship.

Today, I am not an official mentor, but I am as open as I can, I explain things as much as I can, and I bring practices to my team that creates a culture of learning, it gives me the joy to see other devs happy, and most of the devs are happy when they learn.

Having said that, I also acknowledge that having a mentor is an excellent highway to grow our skills. I did it the slow way, but it does not have to be this way.

This article explains what they call: The mentorship diamond, which gives you a visual guide on when and what type of mentor, and mentee you might want/need, as well as the balance between them and your peers.

Is indeed a tool to consider the next time you take your time to reflect on your career.

## Concepts to aid development

#### [Mental model: Rewriting DRY and the rule of three](https://www.patkua.com/blog/rewiring-dry/)

Recently I have been thinking, talking about, and reading about complexity vs abstraction. Adding layers of abstraction in software comes at the cost of adding more complexity.

Complexity is subjective to the knowledge of the person, what for a less experienced developer is complex, might be simple for a more experienced dev.

Therefore, is important for us to re-think the "DRY" principle, as the author of the article proposes, a good approach that I've been following for some years, and that has been working quite well, is the rule of the three. When I write a function, for example, is ok for me to repeat it two times (as I say it: is ok to have two glasses of water), but the moment I have to repeat it one-third time, is the moment when I start thinking: Should I abstract this?

The outcome of using the rule of three here, or if you prefer: to have up to two glasses of water, has resulted over time in me having more control of the abstraction vs complexity.

## I want to read more

#### [Documentation: Diagrams as code](https://www.thoughtworks.com/radar/techniques/diagrams-as-code)

One of the things that an architect has to do, as well as a tech lead, is to document and visualize as clearly as possible the software architecture they are designing. Having a thorough and always up-to-date diagram documentation of systems, is a time-consuming and highly abstract task, mainly because software is not static, the software is always changing, and with every change, we make in the code, we might be making out of date our architecture documentation.

This trend: "diagrams as code", is one of the things that popped out last week on the newsletters I follow. Yet I didn't take a real look yet, but is something I will for sure check out with detail over the next weeks, if there is a way to automate software architecture documentation, and pair it with the code as we write it, I am all in!.

## For my toolbox

#### [HTML: Can I include](https://caninclude.glitch.me/)

Writing valid HTML is one of the important things to keep in mind when building UIs, not only because is "right", but also because valid HTML is better understood by screen readers and different browsers.

Yet I've been working with the web for many years, it is still hard for me to remember what element can I put inside another element.

This is a great reference tool that is going directly to my toolbox, and for sure I will keep using it in the future when I am building UIs

#### [CSS: Less absolute positioning](https://ishadeed.com/article/less-absolute-positioning-modern-css/)

A very easy-to-follow visual guide on how we can position elements in the UI using modern CSS like grid or flexbox.

I found this useful because the visual guide of how to position elements in the UI serves as a mind map, that is easier for me not only to remember but also to have an overview of the myriad of possibilities we have nowadays.

This is one of the guides I will most likely come back to when I am building layouts again in the future.