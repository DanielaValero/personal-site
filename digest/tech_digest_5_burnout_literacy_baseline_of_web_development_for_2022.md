---
title: "Tech digest #5: Burnout literacy & baseline of web development for 2022"
date: 2022-01-30T14:05:06.339Z
summary: >-
  This week:

  Softy: Let’s talk about burnout

  Techy: The baseline for software development in 2022 ・Quality vs Speed (and other models)

  Life: 20 reasons to quit social media

  Note: I also post this on my website.
tags:
  - digest
imageAltText: Fill when you add an image
syndicationTitle: techdigest.substack.com
syndicationLink: https://techdigest.substack.com/p/tech-digest-5-burnout-literacy-and?
---
###### This week:

###### Softy: [Let’s talk about burnout](https://danielavalero.com/posts/part-1:-let's-talk-about-burnout-and-how-to-catch-it-early/)

###### Techy: [The baseline for software development in 2022](https://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022/) ・[Quality vs Speed](https://blackboxofpm.com/product-management-mental-models-for-everyone-31e7828cb50b) (and other models)

###### Life: [20 reasons to quit social media](https://durmonski.com/life-advice/reasons-to-quit-social-media/)

###### Note: I also [post this on my website](https://danielavalero.com/blog/).

- - -

## Softy

### **[Let's talk about burnout and how to catch it early](https://danielavalero.com/posts/part-1:-let's-talk-about-burnout-and-how-to-catch-it-early/)**

Since the pandemic started, many businesses learned that being digital is not only a business strategy but also a survival need.

During the last 2 years, businesses have increased the budget for their digitalization which and therefore the increased workload, deadlines, and pressure to the teams working in it.

Nowadays many tech makers are constantly exposed to acute constant stress and it is more important than ever to talk openly about burnout openly in our teams, and increase our literacy about it so that we have a chance to keep a healthy work-life balance.

In this article, I explain what burnout is, what are the early stages symptoms, and some strategies to mitigate early stages.

[Read the article](https://danielavalero.com/posts/part-1:-let's-talk-about-burnout-and-how-to-catch-it-early/)

- - -

## Techy

### **[The baseline for web development in 2022](https://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022/)**

I like to read these summaries because they give me an overview of what’s going on. The web has become so big, that is rather impossible to keep informed of the details of everything

What I liked about this article:

I liked its invitation to add new constraints to our frontend architectural decisions. Every creative work that has constraints becomes better because they encourage our creativity to search for new connections that we would not have found otherwise.

Go ahead and check it out. Even if you don’t want to read all the words, you can look at the charts and the conclusions, and you will get the same inspiration from the article I did.

[Read the article](https://engineering.linecorp.com/en/blog/the-baseline-for-web-development-in-2022/)

### Concepts: Quality vs. Speed

According to [this article](https://blackboxofpm.com/product-management-mental-models-for-everyone-31e7828cb50b) about mental models for product management, the balance between speed and quality is a function of the confidence you have in the product you will be building.

> The confidence you have in i) the importance of the problem your solving, and ii) the correctness of the solution you’re building, should determine how much you’re willing to trade off speed and quality in a product build.

The higher confidence you have in the solution, the more you should be investing in quality (because you want to scale the product), the lower confidence you have, the more you should invest in speed (because you want to launch it and learn from the users what works and whatnot)

In the article, they explain other mental models, which are interesting and useful for software developers to understand as well, but this one was for me a good one to bring to my bag of tools to use at work.

[Read the article](https://blackboxofpm.com/product-management-mental-models-for-everyone-31e7828cb50b)

- - -

## Life

### **[20 Reasons To Quit Social Media](https://durmonski.com/life-advice/reasons-to-quit-social-media/)**

Getting out of social media, or reducing our consumption of it, forces us to seek and establish real human connections with people around us, start a new hobby, or dedicate more time to an old one.

You will find these and other well-explained reasons to quit social media in this article. Yet, you might already know them all, what I liked from this article is that he articulates the reasons in a short and clear manner, that taking action towards it afterward became easier for me, and I hope for you too.

[Read the article](https://durmonski.com/life-advice/reasons-to-quit-social-media/)

Until the next