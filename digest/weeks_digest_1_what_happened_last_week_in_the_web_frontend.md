---
layout: layouts/post.njk
title: "Week's Digest #1: What happened last week in the web?"
date: 2021-01-22T15:59:43.141Z
summary: I have been reading a bunch of articles, many have gotten my attention,
  two, in particular, have brought me to a new perspective, the first one on how
  to map behaviors with inherent issues, and how they relate to how we write
  software, interact with each other and ultimately write code. The second, on
  how the population of developers increases, how this creates a new field of
  work, Developer Experience, and how developer experience should start
  focussing more on the combination of creating abstractions and helping
  developers to navigate the heterogeneous forest of the complexity of their
  systems
tags:
  - digest
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
### [The Art of Not Taking Things Personally](https://medium.dave-bailey.com/the-art-of-not-taking-things-personally-b7a8395ce172)

Opposite to what the stereotype about software developers says, making software is teamwork. Conway's law says that the structure of the software mirrors the communication structures in an organization, and I might also dare to add that the health of a codebase says how the human interactions and teamwork among the developers who write it are.

This article's title is about not taking things personally, but what I like about it, is that it highlights the underlying issue that might be hidden behind a specific behavior. For example: "Blame is driven by anger".

Think of a team where `git blame` is used as a literal tool of shaming, in a team like this, the tendency is to have a culture of fear, which cuts the learning, growth, and joy of the people working in that team, and has a direct effect on the software they write.

Even if you don't read the article word-to-word, by looking at the headlines, I invite you to reflect on how the underlying issues behind an observable behavior affect the relationships within your team, and therefore the software that you as a team write, and if you want to take a step further, think of how could you address in a proactive and constructive manner these behaviors. I also invite you to think of these behaviors, is there one that you might be having? and what might be the underlying issue?

### [The case for developer experience](https://future.a16z.com/the-case-for-developer-experience)

Over the years I've been working as a web frontend developer, there have been some constants keeping my attention, one of these is to continuously improve the tools, we developers use in order to keep delivering at an accelerating pace while being productive. Nowadays, this area of work has been coined as developer experience (DX).

In this regard, the first point that I found particularly interesting in Jean Yang's article, is that DX, in the same way, UX rose some years ago, arises to respond to the need to create a set of tools that aid developers in their day-to-day work, navigating the complexities of the field, and be able to focus and solve problems with more efficiency. Nowadays, the need for digital transformation comes from the inherent need of companies to become in their insides a tech company, in the same way, Netflix says is not a tech company, but a TV/Film producer.

The bottom line is that the more companies seek to be more digital fluent, the more developers come to the field, so working in developer experience gets a bigger field, meaning this particular area of focus, is slowly transforming from a side work you do in a sprint, to become a specialization for those who like this area.

The second point I find interesting in Jean's argumentation is the need to balance: Abstraction, and Complexity.

> Since meeting developers’ needs is much easier if you’re automating away functionality, it makes sense that the tools that have been catching on are abstraction tools. In this case, good design is often synonymous with good product ergonomics! But design for complexity-exploring tools — which is more about meeting developers where they are — means digesting larger parts of the rainforest that are the developer’s ecosystem.

New tools that abstract complexity, so that we can just work on building our product, is a key piece on the fast-moving pace of the digital world, but we can't hide the fact that the teams that are technically high performing, are a minority of the world. People did not finish understanding what the RAIL model is, and how to use it in the day-to-day world when the Core Web Vitals have come. People didn't finish understanding the basics of Javascript (with jQuery), when they have to switch to react, or Vue, or the new to come framework.

I don't think the response to the problem is on: Let's stop creating tools and abstracting problems, but is it rather the point that Jean Yan is making: Let's create tools that abstract complexity, and that also play well with the complexities of the reality if their systems, meaning helping them to understand and create mental models of their current systems, helping them to solve problems on areas where abstraction is not the quickest answer.