---
title: "Week's digest #4: What happened last week on the web?"
date: 2021-10-25T17:06:00.062Z
summary: Mental health and stress management - Interactive learning guide for
  web frontend developers
tags:
  - digest
imageAltText: Fill when you add an image
syndicationTitle: My substack newsletter
syndicationLink: https://techdigest.substack.com/p/weeks-digest-4-what-happened-last?justPublished=true
---
###### **This week:**

###### [How to keep calm and carry on in high-pressure project times](https://danielavalero.com/posts/how-to-keep-calm-and-carry-on-in-high-pressure-project-times/)

###### [Communication tool: Argue better by signaling your receptiveness with these words](https://psyche.co/ideas/argue-better-by-signalling-your-receptiveness-with-these-words)

###### **[Developer Tea: How to refill my tank?](https://developertea.com/episodes/c1fdeee5-f941-423e-8aaf-0ccbe5103d63)**

###### [Learn to prioritize with this analogy](https://shepleywood.com/news/rocks-pebbles-sand-and-beer)

###### [Music finding of the week: Beats, Not Words - Sol Monk](https://solmonk.bandcamp.com/)

## **What am I thinking?**

During the last weeks I’ve got into one of those high-pressure times, where you have to manage a lot of things at the same time, dependencies, make abstract ideas concrete, make uncertainty certain.

The fact that I have got into this mode, after a long break of these kinds of modes, got me thinking: What changes can I do in how I distribute my time, and the prioritization of my tasks, so that I can stay calm, and relaxed, even when the times are stressful.

On this note, this week’s digest goes on this topic, different resources that inspire me with ideas where people and well-being are on top of the priority list.



### [How to keep calm and carry on in high-pressure project times](https://danielavalero.com/posts/how-to-keep-calm-and-carry-on-in-high-pressure-project-times/)

This blog post was initially what I am thinking of this week, but came long enough to become its own thing.

I share in there how I am now blocking time in my calendar to do nothing, and take time to reflect on what gives me energy, and what drains me, so that I can address things on time, and in a healthy manner.

**Disclaimer**: I am convinced that in order to deliver with high performance and be efficient, being calm, relaxed, having a good work-life balance is a prerequisite.

### [Communication tool: Argue better by signaling your receptiveness with these words](https://psyche.co/ideas/argue-better-by-signalling-your-receptiveness-with-these-words)

In this article, the authors developed a recipe on how to be more receptive to ideas especially in times when you are in disagreement. What I like about this, is that web developers don’t necessarily learn how to have “communication skills” in university, yet we find ourselves very often in the situation where having a good toolbox on this would come very handy. Below is the outline of the recommendations:

> Actively **acknowledge** the other’s perspective using terms such as ‘I understand that…,’; ‘I see your point’; or ‘What I think you are saying is…’
>
> **Affirm** the other person’s views by highlighting areas of agreement, no matter how small or obvious. For example, say ‘I agree that…’ or ‘You’re right about…’
>
> **Hedge** your claims: say ‘I think it’s possible rather than ‘This will happen because…’ (Note: you can soften your own beliefs, but don’t minimize values! Avoid words such as ‘just’, ‘simply’ or ‘only’.)
>
> Phrase your arguments in **positive** rather than negative terms. Say ‘I think it’s helpful to maintain a social distance’ rather than ‘You should not be socializing right now.’
>
> Share your **personal experiences** – especially involving vulnerability – and this will encourage mutual respect. In contrast, reciting explanations or facts you’ve learned can sound argumentative and condescending.

## **Podcast worth Listening**

### **[Developer Tea: How to refill my tank?](https://developertea.com/episodes/c1fdeee5-f941-423e-8aaf-0ccbe5103d63)**

In my working life as a developer, I have, like we all do, periods of time when there is a lot of work, and periods, when work, is mostly chilled.

One question that occupies my mind in the present is: How can I keep calm, joyful, relaxed, and happy, independently of the high or low phase I am having at work?

There are a bunch of strategies I am thinking and applying, for example, meditation, sports, journaling. But even if I do all that, there is no security that I will keep chilled even if I am in a high, therefore I am always open to adding different techniques to the mix, take some out, take some in. For me, there is no silver bullet, I embrace uncertainty and as a human I keep moving, navigating what life brings.

This podcast in general shares ideas that inspire on the particular topic of having a growth mindset. And this episode shares a lovely idea I will start trying: Keeping my energy tank full.

## **Ideas to bring to my project**

#### [Learn to prioritize with this analogy](https://shepleywood.com/news/rocks-pebbles-sand-and-beer)

One thing that is very important, especially in times of high pressure, is to know how to prioritize. Not only for our own time, but also to within our teams, and make sure everyone knows how to do it.

During this year, I and my team are using this analogy to prioritize work, and also to prioritize feedback in pull requests.

I found again somewhere in the wild web, this little story, that explains in a clear manner, in a way that is easy to remember how to prioritize.

If you can adapt it for yourself and your team, I promise you, it will help!

## **For my toolbox**

### **[Interactive web frontend skills learning guide](https://andreasbm.github.io/web-skills/)**

This interactive guide of web frontend skills organizes a big set of web frontend skills in buckets, has a great information architecture in that regard, and has links to tutorials on each of the topics

This one goes to my toolbox because of the following two reasons:

1. The first is to aid to assess technical skills and identify which technical knowledge is important to manage per career stage
2. The second is a go-to, for me to learn further topics I don’t necessarily master, as well as share with fellow colleagues in their journey as web frontend developers.

## **Art for the soul**

### [Music finding of the week: Beats, Not Words - Sol Monk](https://solmonk.bandcamp.com/)

Found last week this album by Sol Monk a drummer and producer born in Jerusalem.

The album is a fusion of Spiritual Jazz, electronic, and maybe even a bit of soul. The album is colorful, interesting, and for me, a good companion to work with.

Sol Monk said about the album what follows:

> "I am very happy to present this album - made with love for music and spiritual search.\
> Rejoicer's effortless and easy approach to the creation process kept the music and workflow fresh, while some of my good friends, who also happen to be my favorite musicians, contributed to the amazing atmosphere in the studio.\
> This music was created first of all in the name of music."-- Sol Monk

[Spotify link](https://open.spotify.com/album/0UGHzhMk7swPy6vjUlaoX5)

[Bandcamp link](https://timegrove.bandcamp.com/album/beats-not-words)