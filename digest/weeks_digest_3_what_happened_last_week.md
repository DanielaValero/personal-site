---
title: "Week's digest #3: What happened last week in the web?"
date: 2021-09-24T07:37:24.775Z
summary: >
  This week's digest is more focused on non-technical tools, things like how to
  evolve a process that works in teams, how to influence without authority, how
  to coordinate. But it has at the end some nice Aww making links to spark our
  joy
tags:
  - digest
imageAltText: Fill when you add an image
syndicationTitle: ""
syndicationLink: ""
---
This week's digest is more focused on non-technical tools because I have not been programming. It has things like how to evolve a process that works in teams, how to influence without authority, how to coordinate. But it has at the end some nice Aww making links to spark our joy

## What I am thinking?

There are so many things that I would like to do or that land on my plate at work. Because I can't and don't want to do everything. I ask myself the following questions to help me prioritize and organize my time:

* **What do I want to own?:** What gives me joy and also makes an impact on the business and customers
* **What do I want to collaborate on?:** What I like, but there is another person or people who are more passionate about it
* **What do I want to be an observer?:** Nice ideas I'd like to see happening
* **What do I want to delegate?:** If there is someone who this challenge would excite, like, or want to do it, then I would engage them
* **What do I want to drop?:** Whatever drains my energy or does not produce an impact
* **What is in the important/urgent and important/non-urgent axis?:** important and urgent get priority, then come important and not urgent.

Finding answers for this, amidst feeling passionate about everything and not wanting to drop anything, can be hard. But it is worth doing, especially because it helps to keep a good work-life balance, which will end up making you a happier person.

## Ideas to bring to my project

### [A good process is evolved, not designed](https://lethain.com/good-process-is-evolved/)

This blog has a bunch of useful content for tech leads or anyone in a similar role. What I liked about this blog post, is that it takes a UX approach to bring new processes to development teams.

This is useful because, we, developers are humans too, are users too, and have in a normal day a lot of things to take care of. And introducing a new process, a new way of working usually works best when you do it together with the rest of your team.

I have been doing this for a while already, mostly intuitively, but this post nailed it. I did a diagram for me, to remember, which I share here with you.

![Steps to evolve a process diagramed out of the linked article](/static/img/screenshot_2021-09-29_at_17.17.00.png)

## Concepts

### [How to influence without authority](https://medium.com/atlassian-product-management-blog/how-to-influence-without-authority-4622ad7d52c9)

We all find ourselves in the situation, from time to time, when we want or need to influence others, but have no authority for that. I would even argue, that if your approach is to be more like a leader, instead of a  manager/boss, even if you are in a position of authority, using tools like these will come in very handy.

What I liked about this blog post, is that it proposes to do the first step that the model [Non-Violent Communication](https://www.nonviolentcommunication.com/learn-nonviolent-communication/4-part-nvc/) suggests: Understand first, or in the NVC model: Listen first.

How many times didn't I come to a new team, project, or situation, where I saw some non-efficient - to my eyes - process, and I proposed directly a solution?, and out of all those times, how many times do you think I succeeded?... Don't answer.

In the post, the author expands on what to do after understanding, but what I will remember and I am now actively practicing is: Understand first. Once I nail this consistently, I will come back to it, and read what is the next step.

### [Coordination models - tools for getting groups to work well together](https://www.rubick.com/coordination-models/)

Software development is a team sport, and if you, like me, are mostly working in a large-enterprise field, you will find yourself needing to collaborate across different teams.

If one thing I have learned from reading, learning, and experimenting over years with psychology and sociology theories, is that labeling things helps to address the ambiguity.

This blog post goes definitely to my toolbox, first because it gives a definition to collaboration models I have used in my career, and second because it adds new useful ones. It will come handy when I find myself in situations where a new collaboration model needs to be established.

## I want to read more

### [Key data structures and their roles in RenderingNG](https://developer.chrome.com/blog/renderingng-data-structures/)

I am one of those people who goes and reads things like EcmaScript specs. This blog post goes a bit into that nerd direction (note that my definition of nerd is positive), I am not sure if I will actually find the time, or when I will, but every time I read and understand how rendering engines in the browser work, I become a better developer. And that lights my fire.

## For my toolbox

### [Copy generator: Speak Human](https://www.speakhuman.today/)

Is a random feedback text generator, which I will most likely come back to, whenever I didn't get yet the copy from the copywriters, UX, or whoever decides what copy to write in a specific part of the web app I am working on.  It gives you messages like the one below to use for example in a loading message.

"Time to take a breathe and smile, how beautiful you are [By Alfathony](https://www.instagram.com/alfathony)" - Perhaps I will add this as an Easter egg for screen readers without telling anyone.

## Pretty, Aww or probably useless

### [CSS: What if focus was not styled with an outline around the field?](https://lab.hakim.se/focussss/)

[Hakim El Hattab](https://twitter.com/hakimel) is a Swedish developer who is experimenting a lot with CSS, animations, among other things. And his last experiment is a lovely, animation to show in which element you have the focus. Probably something we won’t bring to our projects, but will make you smile at least :)

## Video animation for a mental break

### [Animation: Stop motion animation with matches](https://www.youtube.com/watch?v=2chQKSGowjg)

This is a beautifully made stop motion animation by [Tomohiro Okazaki](http://www.swimmingdesign.com/), with matches. If you will watch it, find some nice electronic song of at least 7min, to pair it with, sit back, and chill.