---
layout: layouts/page.njk
title: About
permalink: /about.html
navtitle: About
eleventyExcludeFromCollections: true
tags:
  - nav
---

<article class="about">
  <picture class="about_image">
    <source type="image/webp" srcset="static/img/layout/nani-about.webp">
    <source type="image/jpeg" srcset="static/img/layout/nani-about.jpg">
     <img class="u-photo" 
      src="/static/img/layout/about.jpg"
      alt="Portrait photo of Daniela">
  </picture>


<summary class="about_content">
I master the web frontend.
I love programming, the nerdier, the better.
I have a very well trained "eye for design".
I am a systems thinker. 
I apply social science knowledge to influence teams into building a high performance culture. 
I advocate for WomXn in Tech by being kind an kindly giving support and awareness to those around me.
<br/>

### Other things about my professional self
#### I love tech and building high quality software
#### I lead teams
#### I try my best to be a role model for diversy and inclusion
#### I believe that happy teams produce more value
#### Creativity is the hability to create new things (not only visual)
#### Use non violent communication
#### Practice Self-Insight
#### Use science knowledge to solve problems (also human ones)
#### Use wisely my/our attention
#### Build meaningful people relationships
#### Practice Radical Candor

</summary>


</article>

