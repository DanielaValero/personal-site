---
layout: layouts/base.njk
title: Daniela Valero
description: I am frontend architect, tech lover, caring about people, well being, productivity and business
eleventyExcludeFromCollections: true
permalink: /index.html
navtitle: Home
section: home
tags:
- nav
---

<article class="layoutCard">
  <picture class="layoutCard_image skeleton">
    <source type="image/webp" srcset="/static/img/layout/profile-main.webp">
    <source type="image/jpeg" srcset="/static/img/layout/profile-main.png">
     <img class="u-photo" 
      src="/static/img/layout/profile-main.png"
      alt="Portrait photo of Daniela">
  </picture>

<summary class="layoutCard_content">
   <h1 class="L1">Hello, I am Daniela</h1>
  <p>I am a web maker, looking to create more <a class="link--inline" target="_blank" href="https://www.humanetech.com/course">humane</a> and sustainable digital products, within <a href="https://www.devops-research.com/research.html" target="_blank">high-performance teams</a>, while advocating for <a href="https://libguides.unthsc.edu/dei/intersectionality" target="_blank"> intersectional diversity.</a></p>
  <p>I am also a feminist, womXn in tech supporter, art lover, yogini and sometimes-actress</p>
<h2 class="L3">I Value</h2>
<ul>
  <li>Collaboration</li>
  <li>Growth mindset</li>
  <li>Nurturing relationships & radical candor</li>
  <li>High quality software</li>
  <li>Non violent communication</li>
</ul>

<h3 class="L6">Note</h3>

  <p>These days I write more actively in my <a href="https://notes.danielavalero.com/" target="_blank">second-brain site</a>, than in my blog or my newsletter. If you wish to know what I've been up to, check it out.</p>
</summary>


</article>


