---
layout: layouts/base.njk
title: Digital garden
section: page
description: I am web architect mixing social science, informatics and design. I am also a yoguini and a theater actress in training
tags:
  - footerNav
navtitle: Digital Garden
permalink: /digitalGarden/index.html
---

<h1 class="L1 display">{{ title }}</h1>
<p>I have a huge collection of links, which I use as reference for work, or to share in my newsletter (my very irregular newsletter). Here some of the collections that I find more useful to share. <br><br>Also, just for fun, I share the links of older versions of my website.
</br> 
I started to build a second brain using a mix of P.A.R.A and Zettelkasten. I decided build a mini site out of my notes, because, why not? The repo is:<a href="https://github.com/DanielaValero/learn-in-public/tree/main" target="_blank">https://github.com/DanielaValero/learn-in-public</a>, and the rendered version is: <a href="https://notes.danielavalero.com" target="_blank">My Notes/learn in public rendered site</a>.</p>
<section class="digitalGarden">
  <article class="card">
  <h2 class="L5 display">Hard knowledge</h2>
  <ul class="linkList">
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17879605">Engineering delivery</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17782702">Accessibility</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17782699">Architecture</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17782696">Web Performance</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/23005822">Books and papers</a></li>
  </ul>
  </article>

  <article class="card">
  <h2 class="L5 display">Soft knowledge</h2>
  <ul class="linkList">
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17782706">Managing teams</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/23005862">Managing individuals</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/2137871">Public speaking</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/23005862">Systems thinking</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/21165160">Career frameworks</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/22421604">Lean & Agile</a></li>
     <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/8511853">Book wishlist</a></li>
  </ul>
  </article>

  <article class="card">
  <h2 class="L5 display">Design and references</h2>
  <ul class="linkList">
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/15398094">Inspiration for this website</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/13703571">Types I like</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/17782701">Design tools</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/13584525">More inspiration</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://raindrop.io/collection/8490765">Illustrations</a></li>
  </ul>
  </article>

  <article class="card">
  <h2 class="L5 display">Older versions</h2>
  <ul class="linkList">
    <li><a target="_blank" class=" link-brutal" href="https://v1.danielavalero.com">V1</a></li>
    <li><a target="_blank" class=" link-brutal" href="https://v2019.danielavalero.com">Version 2019</a></li>
  
  </ul>
  </article>

  <!-- <section class="card">
  <h2 class="L5 display">Music</h2>

  <ul class="linkList">
    <li><a target="_blank" class=" link-brutal" href=""></a></li>
    <li><a target="_blank" class=" link-brutal" href=""></a></li>
  </ul>
</section> -->

</div>