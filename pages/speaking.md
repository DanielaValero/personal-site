---
layout: layouts/page.njk
title: Speaking
tags:
  - nav
navtitle: Speaking
permalink: /speaking/index.html
---

<section class="postList h-feed">
<article class="card h-entry blockCard smallInteraction">
  <p class="card_metadata--smaller caption">
    <time class="dt-published" datetime="2020-02-06">
      <small>02-2020 at <a href="https://ctwebdev.de/" rel="author" class="h-card">C’T Web Dev</a></small>
    </time>
  </p>  
  <a class="u-url card_link" target="_blank" rel="noreferrer" href="https://noti.st/danielavalero255/O3tmVI/mental-hacks-to-master-unit-testing">
    <h3 class="p-name card_title L3">Mental hacks to master unit testing</h3>
  </a>
  <ul class="card_summary summary body1">
    <li>∙ Focus point of the talk: How to speak “boss language” to get time to unit test</li>
    <li>∙ How to write accurate unit tests</li>
  </ul>
</article>

<article class="card blockCard smallInteraction">
  <p class="card_metadata--smaller caption">
    <time class="dt-published" datetime="2019-05-15">
      <small>May 2019 at <a href="https://www.meetup.com/Women-Techmakers-Frankfurt_Rhein-Main/" rel="author" class="h-card">Women Tech Makers Frankfurt</a></small>
    </time>
  </p>
  
  <a class="u-url card_link" target="_blank" rel="noreferrer" href="https://noti.st/danielavalero255/3KaOce/debugging-in-the-web-strategies-for-faster-debugging-increased-productivity">
    <h3 class="p-name card_title L3">Strategies for Faster Debugging & Increased Productivity</h3>
  </a>

  <ul class="card_summary summary body1">
    <li>∙ A quick preamble of the concept of professionalism applied to developers</li>
    <li>∙ Three steps to debug</li>
    <li>∙ Live debugging</li>
  </ul>
</article>

<article class="card blockCard smallInteraction">
  <p class="card_metadata--smaller caption">
    <time class="dt-published" datetime="2019-05-17">
      <small>05-2017 at <a href="https://www.meetup.com/sthlm-js/" rel="author" class="h-card">sthlm.js</a></small>
    </time>
  </p>  

  <a class="u-url card_link" target="_blank" rel="noreferrer" href="https://noti.st/danielavalero255/oh8mrq/from-good-intentions-to-reality-understanding-how-high-quality-projects-are-built">
    <h3 class="p-name card_title L3">From good intentions to reality, understanding how quality projects are built</h3>
  </a>

  <ul class="card_summary summary body1">
    <li>∙ How to write high quality word for the real-world developers.</li>
  </ul>
</article>

<article class="card blockCard smallInteraction">
  <p class="card_metadata--smaller caption">
    <time class="dt-published" datetime="2019-10-05">
      <small>10-2015 at <a href="https://publicissapient.com" rel="author" class="h-card">Publicis Sapient</a></small>
    </time>
  </p>  

  <a class="u-url card_link"  target="_blank" rel="noreferrer" href="https://noti.st/danielavalero255/R0rakF/minding-the-gap-in-cross-cultural-teams">
    <h3 class="p-name card_title L3">Minding the gap in cross cultural teams</h3>
  </a>

  <ul class="card_summary summary body1">
    <li>∙ What Cross-Cultural intelligence is</li>
    <li>∙ How to bridge cross cultural gaps</li>
  </ul>
</article>

</section>