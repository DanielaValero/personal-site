---
layout: layouts/page.njk
title: Elements
eleventyExcludeFromCollections: true
date: 2019-08-06T00:00:00.000Z
permalink: /elements.html
navtitle: Elements
tags:
- nav
---


<h2 class="L3">Typography</h2>
<div>===============</div>
<br />

  <h1 class="L1">Headline 1</h1>
  <h2 class="L2">Headline 2</h2>
  <h3 class="L3">Headline 3</h3>
  <h4 class="L4">Headline 4</h4>
  <h5 class="L5">Headline 5</h5>
  <h6 class="L6">Headline 6</h6>
<br />
  <p class="body1">Body 1. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis
    tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos
    laborum fugiat deleniti? Eum quasi quidem quibusdam.</p>
<br />
  <p class="body2">Body 2. Lorem ipsum dolor sit amet consectetur adipisicing elit. Cupiditate aliquid
    ad quas sunt voluptatum officia dolorum cumque, possimus nihil molestias sapiente necessitatibus dolor saepe
    inventore, soluta id accusantium voluptas beatae.</p>
<br />
<div><span class="caption">Caption text</span></div>
<br />
    <strong>Bold with strong </strong> <br />
  <em>italic with em</em><br />
  <b>Bold with b</b><br />
  <i>italic with i</i><br />
<br />

<br />
<h3>Code</h3>
<div>===============</div>
<br />
<code>our code snippet</code>
<br />
<h3>HR </h3>
<div>===============</div>
<hr />

<h3>Links </h3>
<div>===============</div>
<br />
<a href="#">a typical link</a>
<br />
<a class="link-brutal" href="#">← Back to blog</a>
<br />
<p class="body1">Body 1.<a href="#">inline link</a></p>
<br />

<h3>List </h3>
<div>===============</div>
<br />
<ul>
  <li>item in a list</li>
  <li>item in a list</li>
  <li>item in a list</li>
</ul>


<ol>
  <li>item in a list</li>
  <li>item in a list</li>
  <li>item in a list</li>
</ol>

<br />
<br />
<br />
<br />
<br />

<h2 class="L1">Inside a post</h2>
<div>===============</div>
<br />
<div class="post">
  <h3 class="L3">Typography</h3>
  <br />
  <div>
    <h1>Headline 1</h1>
    <h2>Headline 2</h2>
    <h3>Headline 3</h3>
    <h4>Headline 4</h4>
    <h5>Headline 5</h5>
    <h6>Headline 6</h6>
  </div>
<br />
  <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quos blanditiis
      tenetur unde suscipit, quam beatae rerum inventore consectetur, neque doloribus, cupiditate numquam dignissimos
      laborum fugiat deleniti? Eum quasi quidem quibusdam.</p> 
  <strong>Bold with strong </strong> <br />
  <em>italic with em</em><br />
  <b>Bold with b</b><br />
  <i>italic with i</i><br />
<br />

<br />
<h3>Code</h3>
<div>===============</div>
<br />
<code>our code snippet</code>
<br />
<h3>HR </h3>
<div>===============</div>
<hr />

<h3>Links </h3>
<div>===============</div>
<br />
<a href="#">a typical link</a>
<br />
<a class="link-brutal" href="#">← Back to blog</a>
<br />
<p class="body1">Body 1.<a href="#">inline link</a></p>
<br />

<h3>List </h3>
<div>===============</div>
<br />
<ul>
  <li>item in a list</li>
  <li>item in a list</li>
  <li>item in a list</li>
</ul>


<ol>
  <li>item in a list</li>
  <li>item in a list</li>
  <li>item in a list</li>
</ol>

<h3>blockquote </h3>
<div>===============</div>

  <blockquote>
    <p>“Burn-out is a syndrome conceptualized as resulting from chronic workplace stress that has not been
      successfully managed. It is characterized by three dimensions:</p>
    <ul>
      <li>Feelings of energy depletion or exhaustion;</li>
      <li>Increased mental distance from one’s job, or feelings of negativism or cynicism related to one's
        job; and</li>
      <li>Reduced professional efficacy.</li>
    </ul>
    <p>Burn-out refers specifically to phenomena in the occupational context and should not be applied to
      describe experiences in other areas of life.”</p>
  </blockquote>



<h3>Code </h3>
<div>===============</div>
  <pre
    class="language-css"><code class="language-css"><span class="token selector">.nani</span> <span class="token punctuation">{</span><br>	<span class="token property">display</span><span class="token punctuation">:</span> flex<span class="token punctuation">;</span><br>	<span class="token property">flex-direction</span><span class="token punctuation">:</span> column<span class="token punctuation">;</span><br>	<span class="token property">align-items</span><span class="token punctuation">:</span> center<span class="token punctuation">;</span><br>	<span class="token property">margin-top</span><span class="token punctuation">:</span> 40px<span class="token punctuation">;</span><br><span class="token punctuation">}</span></code></pre>

  <pre
    class="language-html"><code class="language-html"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>nani<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><br>	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>face skinColor<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><br>		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>hair<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>			<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>curl<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>smile<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>		<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>glasses<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br>	<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br> <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>neck skinColor<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span><br><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>bodyDaniela<span class="token punctuation">"</span></span><span class="token punctuation">></span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">></span></span></code></pre>



<h3>Images </h3>
<div>===============</div>

  <div class="u-photo"><img src="/static/img/204-big.jpg" alt="Naming and describing things for screen readers"></div>
</div>